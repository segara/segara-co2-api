from datetime import date
from app.utils import calculator

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from app.core import models

from app.schemas.route import RouteCreate, Route

from . import driving, vehicle


def create_route(db:Session, route: RouteCreate) -> models.Route:

    # End user doesn't know the vehicle id, so we get it from the
    # database using the provided vehicle license plate
    vehicle_id = vehicle.get_vehicle_by_license_plate(db, route.vehicle_license_plate).id
    partial = calculator.partial_route(db, vehicle_id, route.distance)

    db_route = models.Route(
        distance=route.distance,
        origin_latitude=route.origin_latitude,
        origin_longitude=route.origin_longitude,
        destination_latitude=route.destination_latitude,
        destination_longitude=route.destination_longitude,
        route_date=route.route_date,
        vehicle_license_plate=route.vehicle_license_plate,
        driving_name=route.driving_name,
        partial_co2=partial)
    db.add(db_route)
    db.commit()
    db.refresh(db_route)
    return db_route


def get_route_by_id(db:Session, id: int) -> models.Route:
    return db.query(models.Route).filter(models.Route.id == id).first()


def get_routes_between_dates(db:Session, date1: date, date2: date) -> models.Route:
    return db.query(models.Route)\
        .filter(models.Route.route_date.between(date1, date2))\
        .order_by(models.Route.route_date.asc())\
        .all()


def get_routes_by_vehicle_id(db:Session, id: str) -> models.Route:
    return db.query(models.Route)\
        .filter(models.Route.vehicle_id == id)\
        .order_by(models.Route.route_date.asc())\
        .all()


def get_routes_by_driving_id(db:Session, id: str) -> models.Route:
    return db.query(models.Route)\
        .filter(models.Route.driving_id == id)\
        .order_by(models.Route.route_date.asc())\
        .all()


def get_routes(db:Session, skip: int = 0, limit: int = 100) -> models.Route:
    return db.query(models.Route).offset(skip).limit(limit).all()


def update_route(db:Session, id: int, route: Route) -> models.Route:
    db_route = get_route_by_id(db, id)
    route_data = jsonable_encoder(db_route)
    route = route.dict(skip_defaults=True)
    for field in route_data:
        if field in route:
            setattr(db_route, field, route[field])
    vehicle_id = vehicle.get_vehicle_by_license_plate(db, db_route.vehicle_license_plate).id
    partial = calculator.partial_route(db, vehicle_id, db_route.distance)
    db_route.partial_co2 = partial
    db.commit()
    db.refresh(db_route)
    return db_route


def delete_route(db:Session, id: int) -> models.Route:
    db_route = get_route_by_id(db, id)
    db.delete(db_route)
    db.commit()
    return db_route


def sum_partials(db:Session, date1, date2):
    return db.query(func.sum(models.Route.partial_co2))\
        .filter(models.Route.route_date.between(date1, date2))\
        .scalar()