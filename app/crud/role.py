from sqlalchemy.orm import Session

from app.core import models

from app.schemas.role import Role


def create_role(db:Session, role: Role) -> models.Role:
    db_role = models.Role(name=role.name)
    db.add(db_role)
    db.commit()
    db.refresh(db_role)
    return db_role


def get_role_by_id(db:Session, id: int) -> models.Role:
    return db.query(models.Role).filter(models.Role.id == id).first()


def get_role_by_name(db:Session, name: str) -> models.Role:
    return db.query(models.Role).filter(models.Role.name == name).first()


def get_roles(db:Session, skip: int = 0, limit: int = 100) -> models.Role:
    return db.query(models.Role).offset(skip).limit(limit).all()


def update_role(db:Session, id: int, role: Role) -> models.Role:
    db_role = get_role_by_id(db, id)
    db_role = models.Role(name=role.name)
    db.commit()
    db.refresh(db_role)
    return db_role


def delete_role(db:Session, id: int) -> models.Role:
    db_role = get_role_by_id(db, id)
    db.delete(db_role)
    db.commit()
    return db_role