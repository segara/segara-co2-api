from datetime import date
from app.utils import calculator

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.core import models

from app.schemas.mobile_combustion_invoice import (
    MobileCombustionInvoiceCreate, MobileCombustionInvoice
    )

from . import fuel


def create_invoice(
    db:Session, invoice: MobileCombustionInvoiceCreate
    ) -> models.MobileCombustionInvoice:
    
    # End user doesn't know the fuel id, so we get it from the database
    # using the provided invoice date and fuel name
    year = invoice.invoice_date.year
    fuel_id = fuel.get_fuel_by_name_and_year(db, invoice.fuel_name, year).id
    
    partial = calculator.partial_combustion_invoice(db, fuel_id, invoice.units)
    
    db_invoice = models.MobileCombustionInvoice(
        invoice_number=invoice.invoice_number,
        invoice_date=invoice.invoice_date,
        units=invoice.units,
        total=invoice.total,
        fuel_id=fuel_id,
        vehicle_license_plate=invoice.vehicle_license_plate,
        partial_co2=partial)
    db.add(db_invoice)
    db.commit()
    db.refresh(db_invoice)
    return db_invoice


def get_invoice_by_id(db:Session, id: int) -> models.MobileCombustionInvoice:
    return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.id == id)\
        .first()


def get_invoice_by_invoice_number(
    db:Session, invoice_number: str
    ) -> models.MobileCombustionInvoice:
    return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.invoice_number == invoice_number)\
        .first()


def get_invoices_between_dates(
    db:Session, date1: date, date2: date
    ) -> models.MobileCombustionInvoice:
    return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.invoice_date.between(date1, date2))\
        .order_by(models.MobileCombustionInvoice.invoice_date.asc())\
        .all()


def get_invoices_by_vehicle_id(db:Session, id: str) -> models.MobileCombustionInvoice:
    return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.vehicle_id == id)\
        .order_by(models.MobileCombustionInvoice.invoice_date.asc())\
        .all()


def get_invoices_by_fuel_id(db:Session, id: int) -> models.MobileCombustionInvoice:
    return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.fuel_id == id)\
        .order_by(models.MobileCombustionInvoice.invoice_date.asc())\
        .all()
    

def get_invoices_by_partial(
    db:Session, partial: float, operator: str
    ) -> models.MobileCombustionInvoice:
    if operator == "gt":
        return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.partial_co2 > partial)\
        .order_by(models.MobileCombustionInvoice.partial_co2.asc())\
        .all()
    if operator == "lt":
        return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.partial_co2 < partial)\
        .order_by(models.MobileCombustionInvoice.partial_co2.asc())\
        .all()
    if operator == "et":
        return db.query(models.MobileCombustionInvoice)\
        .filter(models.MobileCombustionInvoice.partial_co2 == partial)\
        .order_by(models.MobileCombustionInvoice.partial_co2.asc())\
        .all()


def get_invoices(db:Session, skip: int = 0, limit: int = 100) -> models.MobileCombustionInvoice:
    return db.query(models.MobileCombustionInvoice).offset(skip).limit(limit).all()


def update_invoice(
    db:Session, id: int, invoice: MobileCombustionInvoice
    ) -> models.MobileCombustionInvoice:
    db_invoice = get_invoice_by_id(db, id)
    invoice_data = jsonable_encoder(db_invoice)
    invoice = invoice.dict(skip_defaults=True)
    for field in invoice_data:
        if field in invoice:
            setattr(db_invoice, field, invoice[field])
    partial = calculator.partial_combustion_invoice(db, db_invoice.fuel_id, db_invoice.units)
    db_invoice.partial_co2 = partial
    db.commit()
    db.refresh(db_invoice)
    return db_invoice


def delete_invoice(db:Session, id: int) -> models.MobileCombustionInvoice:
    db_invoice = get_invoice_by_id(db, id)
    db.delete(db_invoice)
    db.commit()
    return db_invoice