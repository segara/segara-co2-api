from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import asc

from app.core import models

from app.schemas.facility import FacilityCreate, Facility


def create_facility(db:Session, facility: FacilityCreate) -> models.Facility:
    db_facility = models.Facility(
        name=facility.name,
        latitude=facility.latitude,
        longitude=facility.longitude,
        address=facility.address,
        zip_code=facility.zip_code,
        city=facility.city,
        country=facility.country,
        organisation_name=facility.organisation_name,
        outliers_combustion=facility.outliers_combustion,
        outliers_electricity=facility.outliers_electricity)
    db.add(db_facility)
    db.commit()
    db.refresh(db_facility)
    return db_facility


def get_facility_by_id(db:Session, id: int) -> models.Facility:
    return db.query(models.Facility).filter(models.Facility.id == id).first()


def get_facility_by_name(db:Session, name: str) -> models.Facility:
    return db.query(models.Facility).filter(models.Facility.name == name).first()


def get_facilities_by_zip_code(db:Session, zip_code: str) -> models.Facility:
    return db.query(models.Facility)\
        .filter(models.Facility.zip_code == zip_code)\
        .order_by(models.Facility.name.asc())\
        .all()


def get_facilities_by_city(db:Session, city: str) -> models.Facility:
    return db.query(models.Facility)\
        .filter(models.Facility.city == city)\
        .order_by(models.Facility.name.asc())\
        .all()


def get_facilities_by_country(db:Session, country: str) -> models.Facility:
    return db.query(models.Facility)\
        .filter(models.Facility.country == country)\
        .order_by(models.Facility.name.asc())\
        .all()


def get_facilities_by_organisation(db:Session, id: int) -> models.Facility:
    return db.query(models.Facility)\
        .filter(models.Facility.organisation_id == id)\
        .order_by(models.Facility.name.asc())\
        .all()


def get_facilities(db:Session, skip: int = 0, limit: int = 100) -> models.Facility:
    return db.query(models.Facility).offset(skip).limit(limit).all()


def update_facility(db:Session, id: int, facility: Facility) -> models.Facility:
    db_facility = get_facility_by_id(db, id)
    facility_data = jsonable_encoder(db_facility)
    facility = facility.dict(skip_defaults=True)
    for field in facility_data:
        if field in facility:
            setattr(db_facility, field, facility[field])
    db.commit()
    db.refresh(db_facility)
    return db_facility


def delete_facility(db:Session, id: int) -> models.Facility:
    db_facility = get_facility_by_id(db, id)
    db.delete(db_facility)
    db.commit()
    return db_facility