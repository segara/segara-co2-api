from sqlalchemy.orm import Session

from app.core import models

from app.schemas.driving import DrivingCreate, Driving


def create_driving(db:Session, driving: DrivingCreate) -> models.Driving:
    db_driving = models.Driving(name=driving.name)
    db.add(db_driving)
    db.commit()
    db.refresh(db_driving)
    return db_driving


def get_driving_by_id(db:Session, id: int) -> models.Driving:
    return db.query(models.Driving).filter(models.Driving.id == id).first()


def get_driving_by_name(db:Session, name: str) -> models.Driving:
    return db.query(models.Driving).filter(models.Driving.name == name).first()


def get_drivings(db:Session, skip: int = 0, limit: int = 100) -> models.Driving:
    return db.query(models.Driving).offset(skip).limit(limit).all()


def update_driving(db:Session, id: int, driving: Driving) -> models.Driving:
    db_driving = get_driving_by_id(db, id)
    db_driving.name = driving.name
    db.commit()
    db.refresh(db_driving)
    return db_driving


def delete_driving(db:Session, id: int) -> models.Driving:
    db_driving = get_driving_by_id(db, id)
    db.delete(db_driving)
    db.commit()
    return db_driving