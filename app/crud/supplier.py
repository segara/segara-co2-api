from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.core import models

from app.schemas.supplier import Supplier, SupplierCreate


def create_supplier(db:Session, supplier: SupplierCreate) -> models.Supplier:
    db_supplier = models.Supplier(
        name=supplier.name,
        year=supplier.year,
        guarantee_of_origin=supplier.guarantee_of_origin,
        emission_factor=supplier.emission_factor)
    db.add(db_supplier)
    db.commit()
    db.refresh(db_supplier)
    return db_supplier


def get_supplier_by_id(db:Session, id: int) -> models.Supplier:
    return db.query(models.Supplier).filter(models.Supplier.id == id).first()


def get_supplier_by_name(db:Session, name: str) -> models.Supplier:
    return db.query(models.Supplier)\
        .filter(models.Supplier.name == name)\
        .order_by(models.Supplier.year.asc())\
        .all()


def get_suppliers_by_year(db:Session, year: int) -> models.Supplier:
    return db.query(models.Supplier)\
        .filter(models.Supplier.year == year)\
        .order_by(models.Supplier.name.asc())\
        .all()


def get_supplier_by_name_and_year(db: Session, name: str, year: int) -> models.Supplier:
    return db.query(models.Supplier)\
        .filter(models.Supplier.name == name, models.Supplier.year == year)\
        .first()


def get_suppliers_by_guarantee_of_origin(db:Session, guarantee_of_origin: bool) -> models.Supplier:
    return db.query(models.Supplier)\
        .filter(models.Supplier.guarantee_of_origin == guarantee_of_origin)\
        .order_by(models.Supplier.name.asc())\
        .all()


def get_suppliers_by_emission_factor(
    db:Session, emission_factor: float, operator: str
    ) -> models.Supplier:
    if operator == "gt":
        return db.query(models.Supplier)\
        .filter(models.Supplier.emission_factor > emission_factor)\
        .order_by(models.Supplier.emission_factor.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Supplier)\
        .filter(models.Supplier.emission_factor < emission_factor)\
        .order_by(models.Supplier.emission_factor.asc())\
        .all()
    if operator == "et":
        return db.query(models.Supplier)\
        .filter(models.Supplier.emission_factor == emission_factor)\
        .order_by(models.Supplier.emission_factor.asc())\
        .all()


def get_suppliers(db:Session, skip: int = 0, limit: int = 100) -> models.Supplier:
    return db.query(models.Supplier).offset(skip).limit(limit).all()


def update_supplier(db:Session, id: int, supplier: Supplier) -> models.Supplier:
    db_supplier = get_supplier_by_id(db, id)
    supplier_data = jsonable_encoder(db_supplier)
    supplier = supplier.dict(skip_defaults=True)
    for field in supplier_data:
        if field in supplier:
            setattr(db_supplier, field, supplier[field])
    db.commit()
    db.refresh(db_supplier)
    return db_supplier


def delete_supplier(db:Session, id: int) -> models.Supplier:
    db_supplier = get_supplier_by_id(db, id)
    db.delete(db_supplier)
    db.commit()
    return db_supplier
