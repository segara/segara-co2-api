from sqlalchemy.orm import Session

from app.core import models

from app.schemas.user import UserCreate
from app.core.security import Hasher


def create_user(db: Session, user: UserCreate) -> models.User:
    db_user = models.User(
        email=user.email,
        username=user.username,
        fullname=user.fullname,
        hashed_password=Hasher.get_password_hash(user.password),
        is_active=user.is_active,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user_by_id(db: Session, id: int) -> models.User:
    return db.query(models.User).filter(models.User.id == id).first()


def get_user_by_email(db: Session, email: str) -> models.User:
    return db.query(models.User).filter(models.User.email == email).first()


def get_user_by_username(db: Session, username: str) -> models.User:
    return db.query(models.User).filter(models.User.username == username).first()


def get_active_or_unactive_users(db: Session, is_active: bool) -> models.User:
    return (
        db.query(models.User)
        .filter(models.User.is_active == is_active)
        .order_by(models.User.last_name.asc())
    )


def get_users(db: Session, skip: int = 0, limit: int = 100) -> models.User:
    return db.query(models.User).offset(skip).limit(limit).all()


def update_user(db: Session, id: int, user: UserCreate) -> models.User:
    db_user = get_user_by_id(db, id)
    db_user = models.User(
        email=user.email,
        username=user.username,
        fullname=user.fullname,
        hashed_password=Hasher.get_password_hash(user.password),
        is_active=user.is_active,
    )
    db.commit()
    db.refresh(db_user)
    return db_user


def delete_user(db: Session, id: int) -> models.User:
    db_user = get_user_by_id(db, id)
    db.delete(db_user)
    db.commit()
    return db_user
