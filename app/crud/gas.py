from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import asc

from app.core import models

from app.schemas.gas import Gas


def create_gas(db:Session, gas: Gas) -> models.Gas:
    db_gas = models.Gas(
        name=gas.name,
        mixture=gas.mixture,
        formula=gas.formula,
        global_warming_potential=gas.global_warming_potential)
    db.add(db_gas)
    db.commit()
    db.refresh(db_gas)
    return db_gas


def get_gas_by_id(db:Session, id: int) -> models.Gas:
    return db.query(models.Gas).filter(models.Gas.id == id).first()


def get_gas_by_name(db:Session, name: str) -> models.Gas:
    return db.query(models.Gas).filter(models.Gas.name == name).first()


def get_gas_by_formula(db:Session, formula: str) -> models.Gas:
    return db.query(models.Gas).filter(models.Gas.formula == formula).first()


def get_gases_by_mixture(db:Session, mixture: bool) -> models.Gas:
    return db.query(models.Gas)\
        .filter(models.Gas.mixture == mixture)\
        .order_by(models.Gas.name.asc())\
        .all()


def get_gases_by_gwp(db:Session, gwp: float, operator: str) -> models.Gas:
    if operator == "gt":
        return db.query(models.Gas)\
        .filter(models.Gas.global_warming_potential > gwp)\
        .order_by(models.Gas.global_warming_potential.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Gas)\
        .filter(models.Gas.global_warming_potential < gwp)\
        .order_by(models.Gas.global_warming_potential.asc())\
        .all()
    if operator == "et":
        return db.query(models.Gas)\
        .filter(models.Gas.global_warming_potential == gwp)\
        .order_by(models.Gas.global_warming_potential.asc())\
        .all()


def get_gases(db:Session, skip: int = 0, limit: int = 100) -> models.Gas:
    return db.query(models.Gas).offset(skip).limit(limit).all()


def update_gas(db:Session, id: int, gas: Gas) -> models.Gas:
    db_gas = get_gas_by_id(db, id)
    gas_data = jsonable_encoder(db_gas)
    gas = gas.dict(skip_defaults=True)
    for field in gas_data:
        if field in gas:
            setattr(db_gas, field, gas[field])
    db.commit()
    db.refresh(db_gas)
    return db_gas


def delete_gas(db:Session, id: int) -> models.Gas:
    db_gas = get_gas_by_id(db, id)
    db.delete(db_gas)
    db.commit()
    return db_gas