from sqlalchemy.orm import Session

from app.core import models

from app.schemas.simile import Simile, SimileCreate


def create_simile(db:Session, simile: SimileCreate) -> models.Simile:
    db_simile = models.Simile(name=simile.name, volume=simile.volume)
    db.add(db_simile)
    db.commit()
    db.refresh(db_simile)
    return db_simile


def get_simile_by_id(db:Session, id: int) -> models.Simile:
    return db.query(models.Simile).filter(models.Simile.id == id).first()


def get_simile_by_name(db:Session, name: str) -> models.Simile:
    return db.query(models.Simile).filter(models.Simile.name == name).first()


def get_similes_by_volume(db:Session, volume: float, operator: str) -> models.Simile:
    if operator == "gt":
        return db.query(models.Simile)\
        .filter(models.Simile.volume > volume)\
        .order_by(models.Simile.volume.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Simile)\
        .filter(models.Simile.volume < volume)\
        .order_by(models.Simile.volume.asc())\
        .all()
    if operator == "et":
        return db.query(models.Simile)\
        .filter(models.Simile.volume == volume)\
        .order_by(models.Simile.volume.asc())\
        .all()


def get_similes(db:Session, skip: int = 0, limit: int = 100) -> models.Simile:
    return db.query(models.Simile).offset(skip).limit(limit).all()


def update_simile(
    db:Session, id: int, simile: Simile
    ) -> models.Simile:
    db_simile = get_simile_by_id(db, id)
    simile_data = jsonable_encoder(db_simile)
    simile = simile.dict(skip_defaults=True)
    for field in simile_data:
        if field in simile:
            setattr(db_simile, field, simile[field])
    db.commit()
    db.refresh(db_simile)
    return db_simile


def delete_simile(db:Session, id: int) -> models.Simile:
    db_simile = get_simile_by_id(db, id)
    db.delete(db_simile)
    db.commit()
    return db_simile