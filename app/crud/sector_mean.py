from sqlalchemy.orm import Session

from app.core import models

from app.schemas.sector_mean import SectorMean, SectorMeanCreate


def create_sector_mean(db:Session, sector_mean: SectorMeanCreate) -> models.SectorMean:
    db_sector_mean = models.SectorMean(
        sector_name=sector_mean.sector_name,
        year=sector_mean.year,
        mean=sector_mean.mean)
    db.add(db_sector_mean)
    db.commit()
    db.refresh(db_sector_mean)
    return db_sector_mean


def get_sector_mean_by_id(db:Session, id: int) -> models.SectorMean:
    return db.query(models.SectorMean).filter(models.SectorMean.id == id).first()


def get_sector_means_by_year(db:Session, year: int) -> models.SectorMean:
    return db.query(models.SectorMean)\
        .filter(models.SectorMean.year == year)\
        .order_by(models.SectorMean.sector_name.asc())\
        .all()


def get_sector_means_by_name(db:Session, sector_name: str) -> models.SectorMean:
    return db.query(models.SectorMean)\
        .filter(models.SectorMean.sector_name == sector_name)\
        .order_by(models.SectorMean.sector_name.asc())\
        .all()


def get_sector_mean_by_name_and_year(
    db: Session, sector_name: str, year: int
    ) -> models.SectorMean:
    return db.query(models.SectorMean)\
        .filter(models.SectorMean.sector_name == sector_name, models.SectorMean.year == year)\
        .first()


def get_sector_means(db:Session, skip: int = 0, limit: int = 100) -> models.SectorMean:
    return db.query(models.SectorMean).offset(skip).limit(limit).all()


def update_sector_mean(
    db:Session, id: int, sector_mean: SectorMean
    ) -> models.SectorMean:
    db_sector_mean = get_sector_mean_by_id(db, id)
    sector_mean_data = jsonable_encoder(db_sector_mean)
    sector_mean = sector_mean.dict(skip_defaults=True)
    for field in sector_mean_data:
        if field in sector_mean:
            setattr(db_sector_mean, field, sector_mean[field])
    db.commit()
    db.refresh(db_sector_mean)
    return db_sector_mean


def delete_sector_mean(db:Session, id: int) -> models.SectorMean:
    db_sector_mean = get_sector_mean_by_id(db, id)
    db.delete(db_sector_mean)
    db.commit()
    return db_sector_mean


def get_last_sector_mean(db:Session, sector_name: str) -> models.SectorMean:
    return db.query(models.SectorMean)\
        .filter(models.SectorMean.sector_name == sector_name)\
        .order_by(models.SectorMean.sector_name.desc())\
        .first()