from sqlalchemy.orm import Session

from app.core import models

from app.schemas.sector import Sector, SectorCreate


def create_sector(db:Session, sector: SectorCreate) -> models.Sector:
    db_sector = models.Sector(name=sector.name)
    db.add(db_sector)
    db.commit()
    db.refresh(db_sector)
    return db_sector


def get_sector_by_id(db:Session, id: int) -> models.Sector:
    return db.query(models.Sector).filter(models.Sector.id == id).first()


def get_sector_by_name(db:Session, name: str) -> models.Sector:
    return db.query(models.Sector).filter(models.Sector.name == name).first()


def get_sectors(db:Session, skip: int = 0, limit: int = 100) -> models.Sector:
    return db.query(models.Sector).offset(skip).limit(limit).all()


def update_sector(
    db:Session, id: int, sector: Sector
    ) -> models.Sector:
    db_sector = get_sector_by_id(db, id)
    db_sector.name = sector.name
    db.commit()
    db.refresh(db_sector)
    return db_sector


def delete_sector(db:Session, id: int) -> models.Sector:
    db_sector = get_sector_by_id(db, id)
    db.delete(db_sector)
    db.commit()
    return db_sector