from datetime import date
from app.utils import calculator

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.core import models

from app.schemas.fluorocarbon import Fluorocarbon

from . import gas, equipment


def create_fluorocarbon(db:Session, fluorocarbon: Fluorocarbon) -> models.Fluorocarbon:

    # End user doesn't know the gas id, so we get it from database
    gas_id = gas.get_gas_by_name(db, fluorocarbon.gas_name).id
    partial = calculator.partial_fluorocarbon(db, gas_id, fluorocarbon.annual_refilling)
    
    db_fluorocarbon = models.Fluorocarbon(
        annual_refilling=fluorocarbon.annual_refilling,
        year=fluorocarbon.year,
        equipment_nickname=fluorocarbon.equipment_nickname,
        gas_id=gas_id,
        partial_co2=partial)
    db.add(db_fluorocarbon)
    db.commit()
    db.refresh(db_fluorocarbon)
    return db_fluorocarbon


def get_fluorocarbon_by_id(db:Session, id: int) -> models.Fluorocarbon:
    return db.query(models.Fluorocarbon).filter(models.Fluorocarbon.id == id).first()


def get_fluorocarbons_by_equipment_id(db:Session, id: int) -> models.Fluorocarbon:
    return db.query(models.Fluorocarbon).filter(models.Fluorocarbon.equipment_id == id).all()


def get_fluorocarbons_by_year(db:Session, year: int) -> models.Fluorocarbon:
    return db.query(models.Fluorocarbon).filter(models.Fluorocarbon.year == year).all()


def get_fluorocarbons_by_partial(db:Session, partial: float, operator: str) -> models.Fluorocarbon:
    if operator == "gt":
        return db.query(models.Fluorocarbon)\
        .filter(models.Fluorocarbon.partial_co2 > partial)\
        .order_by(models.Fluorocarbon.partial_co2.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Fluorocarbon)\
        .filter(models.Fluorocarbon.partial_co2 < partial)\
        .order_by(models.Fluorocarbon.partial_co2.asc())\
        .all()
    if operator == "et":
        return db.query(models.Fluorocarbon)\
        .filter(models.Fluorocarbon.partial_co2 == partial)\
        .order_by(models.Fluorocarbon.partial_co2.asc())\
        .all()


def get_fluorocarbons(db:Session, skip: int = 0, limit: int = 100) -> models.Fluorocarbon:
    return db.query(models.Fluorocarbon).offset(skip).limit(limit).all()


def update_fluorocarbon(
    db:Session, id: int, fluorocarbon: Fluorocarbon
    ) -> models.Fluorocarbon:
    db_fluorocarbon = get_fluorocarbon_by_id(db, id)
    fluorocarbon_data = jsonable_encoder(db_fluorocarbon)
    fluorocarbon = fluorocarbon.dict(skip_defaults=True)
    for field in fluorocarbon_data:
        if field in fluorocarbon:
            setattr(db_fluorocarbon, field, fluorocarbon[field])
    partial = calculator.partial_fluorocarbon(
        db, db_fluorocarbon.gas_id, db_fluorocarbon.annual_refilling
        )
    db_fluorocarbon.partial_co2 = partial
    db.commit()
    db.refresh(db_fluorocarbon)
    return db_fluorocarbon


def delete_fluorocarbon(db:Session, id: int) -> models.Fluorocarbon:
    db_fluorocarbon = get_fluorocarbon_by_id(db, id)
    db.delete(db_fluorocarbon)
    db.commit()
    return db_fluorocarbon