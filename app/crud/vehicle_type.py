from sqlalchemy.orm import Session

from app.core import models

from app.schemas.vehicle_type import VehicleType, VehicleTypeCreate


def create_vehicle_type(db:Session, vehicle_type: VehicleTypeCreate) -> models.VehicleType:
    db_vehicle_type = models.VehicleType(name=vehicle_type.name)
    db.add(db_vehicle_type)
    db.commit()
    db.refresh(db_vehicle_type)
    return db_vehicle_type


def get_vehicle_type_by_id(db:Session, id: int) -> models.VehicleType:
    return db.query(models.VehicleType).filter(models.VehicleType.id == id).first()


def get_vehicle_type_by_name(db:Session, name: str) -> models.VehicleType:
    return db.query(models.VehicleType).filter(models.VehicleType.name == name).first()


def get_vehicle_types(db:Session, skip: int = 0, limit: int = 100) -> models.VehicleType:
    return db.query(models.VehicleType).offset(skip).limit(limit).all()


def update_vehicle_type(
    db:Session, id: int, vehicle_type: VehicleType
    ) -> models.VehicleType:
    db_vehicle_type = get_vehicle_type_by_id(db, id)
    db_vehicle_type.name = vehicle_type.name
    db.commit()
    db.refresh(db_vehicle_type)
    return db_vehicle_type


def delete_vehicle_type(db:Session, id: int) -> models.VehicleType:
    db_vehicle_type = get_vehicle_type_by_id(db, id)
    db.delete(db_vehicle_type)
    db.commit()
    return db_vehicle_type