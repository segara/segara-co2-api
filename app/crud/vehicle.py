from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import asc

from app.core import models

from app.schemas.vehicle import Vehicle, VehicleCreate


def create_vehicle(db:Session, vehicle: VehicleCreate) -> models.Vehicle:
    db_vehicle = models.Vehicle(
        license_plate=vehicle.license_plate,
        make=vehicle.make,
        commercial_name=vehicle.commercial_name,
        version=vehicle.version,
        car_year=vehicle.car_year,
        emissions_nedc=vehicle.emissions_nedc,
        emissions_wltp=vehicle.emissions_wltp,
        fleet_name=vehicle.fleet_name,
        vehicle_type_name=vehicle.vehicle_type_name,
        outliers=vehicle.outliers,
        )
    db.add(db_vehicle)
    db.commit()
    db.refresh(db_vehicle)
    return db_vehicle


def get_vehicle_by_id(db:Session, id: int) -> models.Vehicle:
    return db.query(models.Vehicle).filter(models.Vehicle.id == id).first()


def get_vehicle_by_license_plate(db:Session, license_plate: str) -> models.Vehicle:
    return db.query(models.Vehicle).filter(models.Vehicle.license_plate == license_plate).first()


def get_vehicles_by_make(db:Session, make: str) -> models.Vehicle:
    return db.query(models.Vehicle)\
        .filter(models.Vehicle.make == make)\
        .order_by(models.Vehicle.license_plate.asc())\
        .all()


def get_vehicles_by_commercial_name(db:Session, commercial_name: str) -> models.Vehicle:
    return db.query(models.Vehicle)\
        .filter(models.Vehicle.commercial_name == commercial_name)\
        .order_by(models.Vehicle.license_plate.asc())\
        .all()


def get_vehicles_by_car_year(db:Session, car_year: int) -> models.Vehicle:
    return db.query(models.Vehicle)\
        .filter(models.Vehicle.car_year == car_year)\
        .order_by(models.Vehicle.license_plate.asc())\
        .all()


def get_vehicles_by_fleet(db:Session, id: int) -> models.Vehicle:
    return db.query(models.Vehicle)\
        .filter(models.Vehicle.fleet_id == id)\
        .order_by(models.Vehicle.license_plate.asc())\
        .all()


def get_vehicles_by_emissions_nedc(
    db:Session, emissions_nedc: float, operator: str
    ) -> models.Vehicle:
    if operator == "gt":
        return db.query(models.Vehicle)\
        .filter(models.Vehicle.emissions_nedc > emissions_nedc)\
        .order_by(models.Vehicle.emissions_nedc.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Vehicle)\
        .filter(models.Vehicle.emissions_nedc < emissions_nedc)\
        .order_by(models.Vehicle.emissions_nedc.asc())\
        .all()
    if operator == "et":
        return db.query(models.Vehicle)\
        .filter(models.Vehicle.emissions_nedc == emissions_nedc)\
        .order_by(models.Vehicle.emissions_nedc.asc())\
        .all()


def get_vehicles_by_emissions_wltp(
    db:Session, emissions_wltp: float, operator: str
    ) -> models.Vehicle:
    if operator == "gt":
        return db.query(models.Vehicle)\
        .filter(models.Vehicle.emissions_wltp > emissions_wltp)\
        .order_by(models.Vehicle.emissions_wltp.asc())\
        .all()
    if operator == "lt":
        return db.query(models.Vehicle)\
        .filter(models.Vehicle.emissions_wltp < emissions_wltp)\
        .order_by(models.Vehicle.emissions_wltp.asc())\
        .all()
    if operator == "et":
        return db.query(models.Vehicle)\
        .filter(models.Vehicle.emissions_wltp == emissions_wltp)\
        .order_by(models.Vehicle.emissions_wltp.asc())\
        .all()
        

def get_vehicles(db:Session, skip: int = 0, limit: int = 100) -> models.Vehicle:
    return db.query(models.Vehicle).offset(skip).limit(limit).all()


def update_vehicle(db:Session, id: int, vehicle: Vehicle) -> models.Vehicle:
    db_vehicle = get_vehicle_by_id(db, id)
    vehicle_data = jsonable_encoder(db_vehicle)
    vehicle = vehicle.dict(skip_defaults=True)
    for field in vehicle_data:
        if field in vehicle:
            setattr(db_vehicle, field, vehicle[field])
    db.commit()
    db.refresh(db_vehicle)
    return db_vehicle


def delete_vehicle(db:Session, id: int) -> models.Vehicle:
    db_vehicle = get_vehicle_by_id(db, id)
    db.delete(db_vehicle)
    db.commit()
    return db_vehicle