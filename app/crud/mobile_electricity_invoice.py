from datetime import date
from app.utils import calculator

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.core import models

from app.schemas.mobile_electricity_invoice import (
    MobileElectricityInvoiceCreate,
    MobileElectricityInvoice,
)

from . import supplier


def create_invoice(
    db: Session, invoice: MobileElectricityInvoiceCreate
) -> models.MobileElectricityInvoice:

    # End user doesn't know the supplier id, so we get it from
    # the database using the provided invoice date and supplier name
    year = invoice.invoice_date.year
    db_supplier = supplier.get_supplier_by_name_and_year(
        db, invoice.supplier_name, year
    )

    if not db_supplier:
        supplier_id = supplier.get_supplier_by_name_and_year(db, name="MIX", year=year)
    else:
        supplier_id = db_supplier.id

    partial = calculator.partial_electricity_invoice(db, supplier_id, invoice.units)

    db_invoice = models.MobileElectricityInvoice(
        invoice_number=invoice.invoice_number,
        invoice_date=invoice.invoice_date,
        units=invoice.units,
        total=invoice.total,
        supplier_id=supplier_id,
        vehicle_license_plate=invoice.vehicle_license_plate,
        partial_co2=partial,
    )
    db.add(db_invoice)
    db.commit()
    db.refresh(db_invoice)
    return db_invoice


def get_invoice_by_id(db: Session, id: int) -> models.MobileElectricityInvoice:
    return (
        db.query(models.MobileElectricityInvoice)
        .filter(models.MobileElectricityInvoice.id == id)
        .first()
    )


def get_invoice_by_invoice_number(
    db: Session, invoice_number: str
) -> models.MobileElectricityInvoice:
    return (
        db.query(models.MobileElectricityInvoice)
        .filter(models.MobileElectricityInvoice.invoice_number == invoice_number)
        .first()
    )


def get_invoices_between_dates(
    db: Session, date1: date, date2: date
) -> models.MobileElectricityInvoice:
    return (
        db.query(models.MobileElectricityInvoice)
        .filter(models.MobileElectricityInvoice.invoice_date.between(date1, date2))
        .order_by(models.MobileElectricityInvoice.invoice_date.asc())
        .all()
    )


def get_invoices_by_vehicle_id(db: Session, id: str) -> models.MobileElectricityInvoice:
    return (
        db.query(models.MobileElectricityInvoice)
        .filter(models.MobileElectricityInvoice.vehicle_id == id)
        .order_by(models.MobileElectricityInvoice.invoice_date.asc())
        .all()
    )


def get_invoices_by_supplier_id(
    db: Session, id: int
) -> models.MobileElectricityInvoice:
    return (
        db.query(models.MobileElectricityInvoice)
        .filter(models.MobileElectricityInvoice.supplier_id == id)
        .order_by(models.MobileElectricityInvoice.invoice_date.asc())
        .all()
    )


def get_invoices_by_partial(
    db: Session, partial: float, operator: str
) -> models.MobileElectricityInvoice:
    if operator == "gt":
        return (
            db.query(models.MobileElectricityInvoice)
            .filter(models.MobileElectricityInvoice.partial_co2 > partial)
            .order_by(models.MobileElectricityInvoice.partial_co2.asc())
            .all()
        )
    if operator == "lt":
        return (
            db.query(models.MobileElectricityInvoice)
            .filter(models.MobileElectricityInvoice.partial_co2 < partial)
            .order_by(models.MobileElectricityInvoice.partial_co2.asc())
            .all()
        )
    if operator == "et":
        return (
            db.query(models.MobileElectricityInvoice)
            .filter(models.MobileElectricityInvoice.partial_co2 == partial)
            .order_by(models.MobileElectricityInvoice.partial_co2.asc())
            .all()
        )


def get_invoices(
    db: Session, skip: int = 0, limit: int = 100
) -> models.MobileElectricityInvoice:
    return db.query(models.MobileElectricityInvoice).offset(skip).limit(limit).all()


def update_invoice(
    db: Session, id: int, invoice: MobileElectricityInvoice
) -> models.MobileElectricityInvoice:
    db_invoice = get_invoice_by_id(db, id)
    invoice_data = jsonable_encoder(db_invoice)
    invoice = invoice.dict(skip_defaults=True)
    for field in invoice_data:
        if field in invoice:
            setattr(db_invoice, field, invoice[field])
    partial = calculator.partial_electricity_invoice(
        db, db_invoice.supplier_id, db_invoice.units
    )
    db_invoice.partial_co2 = partial
    db.commit()
    db.refresh(db_invoice)
    return db_invoice


def delete_invoice(db: Session, id: int) -> models.MobileElectricityInvoice:
    db_invoice = get_invoice_by_id(db, id)
    db.delete(db_invoice)
    db.commit()
    return db_invoice
