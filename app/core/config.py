class Settings:
    DATABASE_URL = "postgresql://segara_user:123456@localhost:5432/segara"
    SECRET_KEY = "853624639f9e809bb5eb475b6373253405fe82f557f3328594b4aaa7b7e608f4"
    ALGORITHM = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES = 30  # in mins
    ADMIN_USER_EMAIL = "demo@example.com"
    ADMIN_USER_USERNAME = "admin"
    ADMIN_USER_FULLNAME = "Administrador"
    ADMIN_USER_PASSWORD = "123456"
    ORIGINS = "['http://localhost:8000', 'http://localhost:8080']"


settings = Settings()
