from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session

from app import schemas
from app.core import database, models
from app.core.config import Settings
from app.utils import importer
from ..crud import (
    driving,
    equipment_type,
    equipment,
    facility,
    fleet,
    fluorocarbon,
    fuel,
    gas,
    mobile_combustion_invoice,
    mobile_electricity_invoice,
    organisation,
    revenue,
    role,
    route,
    sector_mean,
    sector,
    simile,
    stationary_combustion_invoice,
    stationary_electricity_invoice,
    supplier,
    user,
    vehicle_type,
    vehicle,
)


settings = Settings()
engine = create_engine(settings.DATABASE_URL, pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
directory = "app/static/initial_data/"


def add_default_sectors(db):
    importer.import_csv(
        file=f"{directory}sectors.csv",
        schema=schemas.sector.Sector,
        validate_fn=getattr(sector, "get_sector_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(sector, "create_sector"),
        save_args="sector=record",
        db=db,
    )


def add_default_sector_means(db):
    importer.import_csv(
        file=f"{directory}sector_means.csv",
        schema=schemas.sector_mean.SectorMean,
        validate_fn=getattr(sector_mean, "get_sector_mean_by_name_and_year"),
        validate_args="sector_name=new_object.sector_name,year=new_object.year",
        save_fn=getattr(sector_mean, "create_sector_mean"),
        save_args="sector_mean=record",
        db=db,
    )


def add_default_organisation(db):
    importer.import_csv(
        file=f"{directory}organisation.csv",
        schema=schemas.organisation.Organisation,
        validate_fn=getattr(organisation, "get_organisation_by_name"),
        validate_args="organisation_name=new_object.name",
        save_fn=getattr(organisation, "create_organisation"),
        save_args="organisation=record",
        db=db,
    )


def add_default_revenues(db):
    importer.import_csv(
        file=f"{directory}revenues.csv",
        schema=schemas.revenue.Revenue,
        validate_fn=getattr(revenue, "get_revenue_by_year"),
        validate_args="year=new_object.year",
        save_fn=getattr(revenue, "create_revenue"),
        save_args="revenue=record",
        db=db,
    )


def add_default_similes(db):
    importer.import_csv(
        file=f"{directory}similes.csv",
        schema=schemas.simile.Simile,
        validate_fn=getattr(simile, "get_simile_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(simile, "create_simile"),
        save_args="simile=record",
        db=db,
    )


def add_default_drivings(db):
    importer.import_csv(
        file=f"{directory}drivings.csv",
        schema=schemas.driving.Driving,
        validate_fn=getattr(driving, "get_driving_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(driving, "create_driving"),
        save_args="driving=record",
        db=db,
    )


def add_default_vehicle_types(db):
    importer.import_csv(
        file=f"{directory}vehicle_types.csv",
        schema=schemas.vehicle_type.VehicleType,
        validate_fn=getattr(vehicle_type, "get_vehicle_type_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(vehicle_type, "create_vehicle_type"),
        save_args="vehicle_type=record",
        db=db,
    )


def add_default_equipment_types(db):
    importer.import_csv(
        file=f"{directory}equipment_types.csv",
        schema=schemas.equipment_type.EquipmentType,
        validate_fn=getattr(equipment_type, "get_equipment_type_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(equipment_type, "create_equipment_type"),
        save_args="equipment_type=record",
        db=db,
    )


def add_default_fuels(db):
    importer.import_csv(
        file=f"{directory}fuels.csv",
        schema=schemas.fuel.Fuel,
        validate_fn=getattr(fuel, "get_fuel_by_name_and_year"),
        validate_args="name=new_object.name,year=new_object.year",
        save_fn=getattr(fuel, "create_fuel"),
        save_args="fuel=record",
        db=db,
    )


def add_default_gases(db):
    importer.import_csv(
        file=f"{directory}gases.csv",
        schema=schemas.gas.Gas,
        validate_fn=getattr(gas, "get_gas_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(gas, "create_gas"),
        save_args="gas=record",
        db=db,
    )


def add_default_suppliers(db):
    importer.import_csv(
        file=f"{directory}suppliers.csv",
        schema=schemas.supplier.Supplier,
        validate_fn=getattr(supplier, "get_supplier_by_name_and_year"),
        validate_args="name=new_object.name,year=new_object.year",
        save_fn=getattr(supplier, "create_supplier"),
        save_args="supplier=record",
        db=db,
    )


def add_default_facilities(db):
    importer.import_csv(
        file=f"{directory}facilities.csv",
        schema=schemas.facility.Facility,
        validate_fn=getattr(facility, "get_facility_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(facility, "create_facility"),
        save_args="facility=record",
        db=db,
    )


def add_default_equipments(db):
    importer.import_csv(
        file=f"{directory}equipments.csv",
        schema=schemas.equipment.Equipment,
        validate_fn=getattr(equipment, "get_equipment_by_nickname"),
        validate_args="nickname=new_object.nickname",
        save_fn=getattr(equipment, "create_equipment"),
        save_args="equipment=record",
        db=db,
    )


def add_default_fleets(db):
    importer.import_csv(
        file=f"{directory}fleets.csv",
        schema=schemas.fleet.Fleet,
        validate_fn=getattr(fleet, "get_fleet_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(fleet, "create_fleet"),
        save_args="fleet=record",
        db=db,
    )


def add_default_vehicles(db):
    importer.import_csv(
        file=f"{directory}vehicles.csv",
        schema=schemas.vehicle.Vehicle,
        validate_fn=getattr(vehicle, "get_vehicle_by_license_plate"),
        validate_args="license_plate=new_object.license_plate",
        save_fn=getattr(vehicle, "create_vehicle"),
        save_args="vehicle=record",
        db=db,
    )


def add_default_mobile_combustion_invoices(db):
    importer.import_csv(
        file=f"{directory}mobile_combustion_invoices.csv",
        schema=schemas.mobile_combustion_invoice.MobileCombustionInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(mobile_combustion_invoice, "create_invoice"),
        save_args="invoice=record",
        db=db,
    )


def add_default_routes(db):
    importer.import_csv(
        file=f"{directory}routes.csv",
        schema=schemas.route.Route,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(route, "create_route"),
        save_args="route=record",
        db=db,
    )


def add_default_stationary_combustion_invoices(db):
    importer.import_csv(
        file=f"{directory}stationary_combustion_invoices.csv",
        schema=schemas.stationary_combustion_invoice.StationaryCombustionInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(stationary_combustion_invoice, "create_invoice"),
        save_args="invoice=record",
        db=db,
    )


def add_default_fluorocarbons(db):
    importer.import_csv(
        file=f"{directory}fluorocarbons.csv",
        schema=schemas.fluorocarbon.Fluorocarbon,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(fluorocarbon, "create_fluorocarbon"),
        save_args="fluorocarbon=record",
        db=db,
    )


def add_default_mobile_electricity_invoices(db):
    importer.import_csv(
        file=f"{directory}mobile_electricity_invoices.csv",
        schema=schemas.mobile_electricity_invoice.MobileElectricityInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(mobile_electricity_invoice, "create_invoice"),
        save_args="invoice=record",
        db=db,
    )


def add_default_stationary_electricity_invoices(db):
    importer.import_csv(
        file=f"{directory}stationary_electricity_invoices.csv",
        schema=schemas.stationary_electricity_invoice.StationaryElectricityInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(stationary_electricity_invoice, "create_invoice"),
        save_args="invoice=record",
        db=db,
    )


def add_default_user(db):
    default_user = schemas.user.UserCreate(
        email=settings.ADMIN_USER_EMAIL,
        username=settings.ADMIN_USER_USERNAME,
        fullname=settings.ADMIN_USER_FULLNAME,
        password=settings.ADMIN_USER_PASSWORD,
    )
    user.create_user(db, user=default_user)


def init_db(db: Session) -> None:
    add_default_sectors(db)
    add_default_sector_means(db)
    add_default_organisation(db)
    add_default_revenues(db)
    add_default_similes(db)
    add_default_drivings(db)
    add_default_vehicle_types(db)
    add_default_equipment_types(db)
    add_default_fuels(db)
    add_default_gases(db)
    add_default_suppliers(db)
    add_default_facilities(db)
    add_default_equipments(db)
    add_default_fleets(db)
    add_default_vehicles(db)
    add_default_mobile_combustion_invoices(db)
    add_default_routes(db)
    add_default_stationary_combustion_invoices(db)
    add_default_fluorocarbons(db)
    add_default_mobile_electricity_invoices(db)
    add_default_stationary_electricity_invoices(db)
    add_default_user(db)


def main() -> None:
    db = SessionLocal()
    init_db(db)


if __name__ == "__main__":
    main()
