from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, Float, ARRAY
from sqlalchemy.orm import relationship

from .database import Base


class Organisation(Base):
    __tablename__ = "organisations"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    logo = Column(String, default="app/static/logo.png")
    sector_name = Column(String, ForeignKey("sectors.name"))
    base_year = Column(Integer)

    facilities = relationship("Facility", back_populates="organisation")


class Sector(Base):
    __tablename__ = "sectors"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)


class SectorMean(Base):
    __tablename__ = "sector_means"

    id = Column(Integer, primary_key=True)
    sector_name = Column(String, ForeignKey("sectors.name"))
    year = Column(Integer)
    mean = Column(Float)


class Simile(Base):
    __tablename__ = "similes"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    volume = Column(Float)  # En m³


class Revenue(Base):
    __tablename__ = "revenues"

    id = Column(Integer, primary_key=True)
    year = Column(Integer)
    total = Column(Float)


class Fleet(Base):
    __tablename__ = "fleets"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    facility_name = Column(String, ForeignKey("facilities.name"))

    vehicles = relationship("Vehicle", back_populates="fleet")


class Vehicle(Base):
    __tablename__ = "vehicles"

    id = Column(Integer, primary_key=True)
    license_plate = Column(String, unique=True)
    make = Column(String)
    commercial_name = Column(String)
    version = Column(String)
    car_year = Column(Integer)
    emissions_nedc = Column(Float)
    emissions_wltp = Column(Float)
    fleet_name = Column(String, ForeignKey("fleets.name"))
    vehicle_type_name = Column(String, ForeignKey("vehicle_types.name"))
    outliers = Column(ARRAY(Float))

    fleet = relationship("Fleet", back_populates="vehicles")
    mobile_combustion_invoices = relationship("MobileCombustionInvoice")
    mobile_electricity_invoices = relationship("MobileElectricityInvoice")
    routes = relationship("Route")


class VehicleType(Base):
    __tablename__ = "vehicle_types"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)


class MobileCombustionInvoice(Base):
    __tablename__ = "mobile_combustion_invoices"

    id = Column(Integer, primary_key=True)
    invoice_number = Column(String)
    invoice_date = Column(Date)
    units = Column(Float)
    total = Column(Float)
    fuel_id = Column(Integer, ForeignKey("fuels.id"))
    vehicle_license_plate = Column(String, ForeignKey("vehicles.license_plate"))
    partial_co2 = Column(Float)

    vehicle = relationship("Vehicle", back_populates="mobile_combustion_invoices")
    fuel = relationship("Fuel")


class MobileElectricityInvoice(Base):
    __tablename__ = "mobile_electricity_invoices"

    id = Column(Integer, primary_key=True)
    invoice_number = Column(String)
    invoice_date = Column(Date)
    units = Column(Float)
    total = Column(Float)
    supplier_id = Column(Integer, ForeignKey("suppliers.id"))
    vehicle_license_plate = Column(String, ForeignKey("vehicles.license_plate"))
    partial_co2 = Column(Float)

    vehicle = relationship("Vehicle", back_populates="mobile_electricity_invoices")
    supplier = relationship("Supplier")


class Route(Base):
    __tablename__ = "routes"

    id = Column(Integer, primary_key=True)
    distance = Column(Float)
    origin_latitude = Column(Float)
    origin_longitude = Column(Float)
    destination_latitude = Column(Float)
    destination_longitude = Column(Float)
    route_date = Column(Date)
    vehicle_license_plate = Column(String, ForeignKey("vehicles.license_plate"))
    driving_name = Column(String, ForeignKey("drivings.name"))
    partial_co2 = Column(Float)

    vehicle = relationship("Vehicle", back_populates="routes")
    driving = relationship("Driving")


class Driving(Base):
    __tablename__ = "drivings"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    routes = relationship("Route", back_populates="driving")


class Facility(Base):
    __tablename__ = "facilities"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    latitude = Column(Float)
    longitude = Column(Float)
    address = Column(String)
    zip_code = Column(String)
    city = Column(String)
    country = Column(String)
    organisation_name = Column(String, ForeignKey("organisations.name"))
    outliers_combustion = Column(ARRAY(Float))
    outliers_electricity = Column(ARRAY(Float))

    equipments = relationship("Equipment", back_populates="facility")
    stationary_combustion_invoices = relationship("StationaryCombustionInvoice")
    stationary_electricity_invoices = relationship("StationaryElectricityInvoice")
    organisation = relationship("Organisation", back_populates="facilities")


class Equipment(Base):
    __tablename__ = "equipments"

    id = Column(Integer, primary_key=True)
    nickname = Column(String, unique=True)
    facility_name = Column(String, ForeignKey("facilities.name"))
    equipment_type_name = Column(String, ForeignKey("equipment_types.name"))
    outliers = Column(ARRAY(Float))

    facility = relationship("Facility", back_populates="equipments")
    fluorocarbons = relationship("Fluorocarbon")


class EquipmentType(Base):
    __tablename__ = "equipment_types"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)


class Fluorocarbon(Base):
    __tablename__ = "fluorocarbons"

    id = Column(Integer, primary_key=True)
    annual_refilling = Column(Float)
    year = Column(Integer)
    equipment_nickname = Column(String, ForeignKey("equipments.nickname"))
    gas_id = Column(Integer, ForeignKey("gases.id"))
    partial_co2 = Column(Float)

    equipment = relationship("Equipment", back_populates="fluorocarbons")
    gas = relationship("Gas")


class Gas(Base):
    __tablename__ = "gases"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    mixture = Column(Boolean())
    formula = Column(String)
    global_warming_potential = Column(Float)

    fluorocarbons = relationship("Fluorocarbon", back_populates="gas")


class StationaryCombustionInvoice(Base):
    __tablename__ = "stationary_combustion_invoices"

    id = Column(Integer, primary_key=True)
    invoice_number = Column(String, nullable=False)
    invoice_date = Column(Date, nullable=False)
    units = Column(Float, nullable=False)
    total = Column(Float)
    fuel_id = Column(Integer, ForeignKey("fuels.id"), nullable=False)
    facility_name = Column(String, ForeignKey("facilities.name"), nullable=False)
    partial_co2 = Column(Float, nullable=False)

    facility = relationship("Facility", back_populates="stationary_combustion_invoices")
    fuel = relationship("Fuel")


class StationaryElectricityInvoice(Base):
    __tablename__ = "stationary_electricity_invoices"

    id = Column(Integer, primary_key=True)
    invoice_number = Column(String, nullable=False)
    invoice_date = Column(Date, nullable=False)
    units = Column(Float, nullable=False)
    total = Column(Float)
    supplier_id = Column(Integer, ForeignKey("suppliers.id"), nullable=False)
    facility_name = Column(String, ForeignKey("facilities.name"), nullable=False)
    partial_co2 = Column(Float, nullable=False)

    facility = relationship(
        "Facility", back_populates="stationary_electricity_invoices"
    )
    supplier = relationship("Supplier")


class Fuel(Base):
    __tablename__ = "fuels"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    year = Column(Integer)
    emission_factor = Column(Float)

    mobile_combustion_invoices = relationship(
        "MobileCombustionInvoice", back_populates="fuel"
    )
    stationary_combustion_invoices = relationship(
        "StationaryCombustionInvoice", back_populates="fuel"
    )


class Supplier(Base):
    __tablename__ = "suppliers"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    year = Column(Integer, nullable=False)
    guarantee_of_origin = Column(Boolean(), nullable=False)
    emission_factor = Column(Float, nullable=False)

    stationary_electricity_invoices = relationship(
        "StationaryElectricityInvoice", back_populates="supplier"
    )
    mobile_electricity_invoices = relationship(
        "MobileElectricityInvoice", back_populates="supplier"
    )


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, nullable=False)
    fullname = Column(String, nullable=False)
    email = Column(String, nullable=False, unique=True, index=True)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)


class Role(Base):
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
