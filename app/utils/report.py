import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import locale
import re
from datetime import date

from sqlalchemy import create_engine

from ..core.config import settings

from ..crud import (
    simile,
    organisation,
    revenue,
    sector_mean,
    mobile_combustion_invoice,
    route,
    stationary_combustion_invoice,
    fluorocarbon,
    mobile_electricity_invoice,
    stationary_electricity_invoice,
)


locale.setlocale(locale.LC_ALL, "es_ES.utf8")
engine = create_engine(settings.DATABASE_URL, pool_pre_ping=True)


class Report:
    def __init__(self, db, simile_name, year, organisation_name):
        self.db = db
        self.year = year
        self.conversion_factor = 0.541  # 1 kg de CO2 ocupa 0,541 m³
        self.simile = simile.get_simile_by_name(db, name=simile_name)
        self.organisation = organisation.get_organisation_by_name(
            db, organisation_name=organisation_name
        )
        # En miles de euros
        self.revenue_this_year = revenue.get_revenue_by_year(db, year=year).total
        # En miles de euros
        self.revenue_base_year = revenue.get_revenue_by_year(
            db, year=self.organisation.base_year
        ).total
        self.total_this_year = 0
        self.total_base_year = 0

    def sector_mean(self):
        mean = sector_mean.get_sector_mean_by_name_and_year(
            db=self.db,
            sector_name=self.organisation.sector_name,
            year=self.organisation.base_year,
        )
        if mean:
            return mean
        else:
            last_mean = sector_mean.get_last_sector_mean(
                db=self.db, sector_name=self.organisation.sector_name
            )
            if not last_mean:
                return False
            return last_mean

    def dataframe(self):
        inventories = [
            {
                "name": "A1 - Vehículos",
                "table": "mobile_combustion_invoices",
                "scope": "A1",
                "column": "invoice_date",
            },
            {
                "name": "A1 - Vehículos",
                "table": "routes",
                "scope": "A1",
                "column": "route_date",
            },
            {
                "name": "A1 - Instalaciones",
                "table": "stationary_combustion_invoices",
                "scope": "A1",
                "column": "invoice_date",
            },
            {
                "name": "A1 - Fluorados",
                "table": "fluorocarbons",
                "scope": "A1",
                "column": "year",
            },
            {
                "name": "A2 - Vehículos",
                "table": "mobile_electricity_invoices",
                "scope": "A2",
                "column": "invoice_date",
            },
            {
                "name": "A2 - Instalaciones",
                "table": "stationary_electricity_invoices",
                "scope": "A2",
                "column": "invoice_date",
            },
        ]

        subdataframes = []
        mobile_combustions = []

        for inventory in inventories:
            df = pd.read_sql_table(inventory["table"], engine)
            if inventory["column"] != "year":
                df = (
                    df.groupby(df[inventory["column"]].map(lambda x: x.year))[
                        "partial_co2"
                    ]
                    .sum()
                    .reset_index()
                )
            else:
                df = df.groupby(df.year)["partial_co2"].sum().reset_index()
            df.columns = ["Año", "Toneladas"]
            scopes = inventory["scope"]
            names = inventory["name"]
            totals = df.assign(**{"Alcance": scopes, "Inventario": names})
            if inventory["name"] == "A1 - Vehículos":
                mobile_combustions.append(totals)
            else:
                subdataframes.append(totals)

        mobile_combustion = pd.concat(mobile_combustions)
        mobile_combustion = (
            mobile_combustion.groupby(["Año", "Alcance", "Inventario"])["Toneladas"]
            .sum()
            .reset_index()
        )
        subdataframes.insert(0, mobile_combustion)

        dataframe = pd.concat(subdataframes)
        dataframe["Toneladas"] = dataframe["Toneladas"].astype(int) / 1000

        table_this_year = dataframe.query(f"Año == {self.year}").filter(
            ["Inventario", "Toneladas"]
        )
        self.total_this_year = table_this_year["Toneladas"].sum()

        table_base_year = dataframe.query(
            f"Año == {self.organisation.base_year}"
        ).filter(["Inventario", "Toneladas"])
        self.total_base_year = table_base_year["Toneladas"].sum()

        return dataframe


class StandardReport(Report):
    def __init__(self, db, simile_name, year, organisation_name):
        super().__init__(db, simile_name, year, organisation_name)
        self.table = self._create_table(self.dataframe())
        self.organisation_name = self.organisation.name
        self.simile_name = self.simile.name
        # Summary table of GHG emissions
        # Regex removes default "dataframe" class from HTML table
        self.results = re.sub(
            'class="dataframe" ',
            'class="',
            self.table.to_html(
                classes="table is-striped", index=False, justify="left", border=0
            ),
        )
        self.base_year = self.organisation.base_year
        self.revenue = {
            f"{self.base_year}": self.revenue_base_year,
            f"{self.year}": self.revenue_this_year,
        }
        self.formatted_total = locale.format("%d", self.total_this_year, grouping=True)
        self.balance_this_year = round(self.total_this_year / self.revenue_this_year, 2)
        self.balance_base_year = round(self.total_base_year / self.revenue_base_year, 2)

    def _create_table(self, data):
        table = data.query(f"Año == {self.year}").filter(["Inventario", "Toneladas"])
        total = table["Toneladas"].sum()
        percentages = [
            round(partial / total * 100, 2) for partial in table["Toneladas"]
        ]
        table["Porcentaje"] = percentages
        row = {"Inventario": "TOTAL", "Toneladas": total, "Porcentaje": 100.00}
        table = table.append(row, ignore_index=True)

        return table

    def _plot_donut(self, table):
        plt.clf()
        fig, ax = plt.subplots()

        size = 0.3  # Used to transform pie into donut

        table = table[:-1]  # Strips last row, since we don't need totals
        labels = table["Inventario"]
        vals = table["Porcentaje"]
        colors = ["#0b486b", "#0b486b", "#0b486b", "#79bd9a", "#79bd9a"]

        fig1, ax1 = plt.subplots()

        patches, texts, autotexts = ax1.pie(
            vals,
            colors=colors,
            labels=labels,
            autopct="%1.1f%%",
            pctdistance=0.85,
            startangle=45,
            wedgeprops=dict(width=size, linewidth=3, edgecolor="w"),
        )

        for text in texts:
            text.set_color("grey")

        i = 0  # Used to hidden text[i] if autotexts[i] <= 0
        for autotext in autotexts:
            autotext.set_color("white")
            # Hiddens value if value < 3
            if float(autotext.get_text().strip("%")) < 3 > 0:
                autotext.set_visible(False)
            # Hiddens label if value <= 0
            if float(autotext.get_text().strip("%")) <= 0:
                texts[i].set_visible(False)
            i += 1

        plt.axis("equal")
        plt.tight_layout()

        return plt

    # def _plot_stacked_bars_chart(self, data):
    #     plt.clf()
    #     fig, ax = plt.subplots()

    #     # Customizes stacked bars colors
    #     colors = ['#0b486b', '#79bd9a']
    #     bottom = np.zeros(len(data))
    #     for i, col in enumerate(data.columns):
    #         ax.bar(
    #             data.index,
    #             data[col],
    #             bottom=bottom,
    #             label=col,
    #             color=colors[i]
    #             )
    #         bottom += np.array(data[col])

    #     # Displays totals
    #     totals = data.sum(axis=1)
    #     offset = max(totals)/20
    #     for i, total in enumerate(totals):
    #         ax.text(
    #             totals.index[i],
    #             total + offset,
    #             round(total),
    #             ha='center',
    #             color='grey')

    #     # Displays subtotals
    #     for bar in ax.patches:
    #         height = bar.get_height()
    #         width = bar.get_width()
    #         x = bar.get_x()
    #         y = bar.get_y()
    #         label_x = x + width / 2
    #         label_y = y + height / 2
    #         subtotal = f'{height:1.0f}'

    #         ax.text(
    #             label_x,
    #             label_y,
    #             subtotal,
    #             ha='center',
    #             color='w',)

    #     # Creates a legend without border and with grey labels
    #     legend = ax.legend(frameon=False)
    #     for text in legend.get_texts():
    #         text.set_color('grey')

    #     # Removes frames and Y axis labels
    #     ax.spines['top'].set_visible(False)
    #     ax.spines['right'].set_visible(False)
    #     ax.spines['bottom'].set_visible(False)
    #     ax.spines['left'].set_visible(False)
    #     ax.get_yaxis().set_visible(False)

    #     # Changes X axis labels color
    #     ax.tick_params(axis='x', colors='grey')

    #     return plt

    def _plot_grouped_chart(self, data):
        plt.clf()
        fig, ax = plt.subplots()

        ax = data.plot.bar(color=["#0b486b", "#79bd9a"], rot=0, xlabel="")

        ax.set_title("En tCO₂eq", color="grey")

        for p in ax.patches:
            ax.annotate(
                format(p.get_height()),
                (p.get_x() + p.get_width() / 2.0, p.get_height()),
                ha="center",
                va="center",
                xytext=(0, 9),
                textcoords="offset points",
                color="grey",
            )

        # Creates a legend without border and with grey labels
        legend = ax.legend(frameon=False)
        for text in legend.get_texts():
            text.set_color("grey")

        # Removes frames and Y axis labels
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)
        ax.get_yaxis().set_visible(False)

        # Changes X axis labels color
        ax.tick_params(axis="x", colors="grey")

        return plt

    def _plot_bar_chart(self, data, title=None):
        plt.clf()
        fig, ax = plt.subplots()

        ax = data.plot.bar(rot=0, color="#607D8B", legend=None)

        if title:
            ax.set_title(title, color="grey")

        for p in ax.patches:
            ax.annotate(
                format(p.get_height()),
                (p.get_x() + p.get_width() / 2.0, p.get_height()),
                ha="center",
                va="center",
                xytext=(0, 9),
                textcoords="offset points",
                color="grey",
            )

        # Removes frames and Y axis labels
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)
        ax.get_yaxis().set_visible(False)

        # Changes X axis labels color
        ax.tick_params(axis="x", colors="grey")

        return plt

    def comparison(self):
        comparison = (
            self.total_this_year * 1000 * self.conversion_factor / self.simile.volume
        )
        return locale.format("%.2f", comparison, grouping=True)

    # Percentage distribution of GHG inventories
    def distribution(self):
        distribution = "tmp/distribution.png"
        self._plot_donut(self.table).savefig(f"app/static/{distribution}")
        return distribution

    # Yearly evolution of GHG emissions
    # def yearly_evolution(self):
    #     yearly_evolution = "yearly_evolution.png"
    #     ghg_by_scopes = self.dataframe().groupby(['Año', 'Alcance'])['Toneladas']\
    #         .sum().unstack().fillna(0)
    #     self._plot_stacked_bars_chart(ghg_by_scopes)\
    #         .savefig(f"app/static/tmp/{yearly_evolution}")
    #     return yearly_evolution

    # Base year comparison
    def base_year_comparison(self):
        if self.base_year < self.year:
            base_year_comparison = "tmp/base_year_comparison.png"
            ghg_base_current = (
                self.dataframe()
                .query(f"Año == {self.year} | Año == {self.base_year}")
                .groupby(["Año", "Alcance"])["Toneladas"]
                .sum()
                .unstack()
                .fillna(0)
            )
            self._plot_grouped_chart(ghg_base_current).savefig(
                f"app/static/{base_year_comparison}"
            )
            return base_year_comparison

    # Base year revenue comparison
    def base_year_revenue_comparison(self):
        if self.base_year < self.year:
            base_year_revenue_comparison = "tmp/base_year_revenue_comparison.png"
            revenue_comparison = pd.DataFrame(
                [self.balance_base_year, self.balance_this_year],
                index=[self.base_year, self.year],
            )
            self._plot_bar_chart(revenue_comparison, title="En tCO₂eq/1k€").savefig(
                f"app/static/{base_year_revenue_comparison}"
            )
            return base_year_revenue_comparison

    def sector_comparison_df(self):
        if self.sector_mean():
            sector = pd.DataFrame(
                {
                    "Sector": self.organisation.sector_name,
                    "tCO₂eq/1k€": self.sector_mean().mean,
                    "Año": [self.sector_mean().year],
                }
            ).set_index("Sector")
            organisation = pd.DataFrame(
                {
                    "Empresa": self.organisation_name,
                    "tCO₂eq/1k€": self.balance_this_year,
                    "Año": [self.year],
                }
            ).set_index("Empresa")
            return pd.concat([sector, organisation])

    # Sector comparison table
    def sector_table(self):
        if self.sector_mean():
            return re.sub(
                'class="dataframe" ',
                'class="',
                self.sector_comparison_df().to_html(
                    classes="table is-striped", index=True, justify="left", border=0
                ),
            )

    # Sector comparison chart
    def sector_chart(self):
        if self.sector_mean():
            sector_chart = "tmp/sector_comparison_chart.png"
            sector_comparison = (
                self.sector_comparison_df()
                .filter(["tCO₂eq/1k€"])
                .set_axis(["Sector", "Empresa"])
            )
            self._plot_bar_chart(sector_comparison).savefig(
                f"app/static/{sector_chart}"
            )
            return sector_chart
