from app.core import models


def partial_route(db, vehicle_id, distance):
    vehicle = db.query(models.Vehicle).filter(models.Vehicle.id == vehicle_id).first()

    if vehicle.emissions_wltp:
        emission_factor = vehicle.emissions_wltp
    else:
        emission_factor = vehicle.emissions_nedc

    # The car emission factor is expressed in g, so we divide it by 1000
    return round(emission_factor * distance / 1000, 2)


def partial_combustion_invoice(db, fuel_id, units):
    fuel = db.query(models.Fuel).filter(models.Fuel.id == fuel_id).first()

    return round(fuel.emission_factor * units, 2)


def partial_electricity_invoice(db, supplier_id, units):
    supplier = (
        db.query(models.Supplier).filter(models.Supplier.id == supplier_id).first()
    )

    if supplier.guarantee_of_origin is False:
        return round(supplier.emission_factor * units, 2)
    else:
        return 0


def partial_fluorocarbon(db, gas_id, refilling):
    gas = db.query(models.Gas).filter(models.Gas.id == gas_id).first()

    return round(gas.global_warming_potential * refilling, 2)
