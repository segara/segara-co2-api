from typing import List, Any

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..core import dependencies, models
from ..utils import importer
from ..crud import facility
from .login import oauth2_scheme

from app.schemas.facility import Facility, FacilityRead, FacilityCreate

import json


router = APIRouter()


@router.post("/facilities/create/", response_model=FacilityRead)
def create_facility(
    new_facility: FacilityCreate,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_facility = facility.get_facility_by_name(db, name=new_facility.name)
    if db_facility:
        raise HTTPException(status_code=400, detail="Facility already registered")
    return facility.create_facility(db=db, facility=new_facility)


@router.post("/facilities/import/all", response_model=List[FacilityRead])
def import_facilities(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    facilities = importer.import_csv(
        file=file,
        schema=Facility,
        validate_fn=getattr(facility, "get_facility_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(facility, "create_facility"),
        save_args="facility=record",
        db=db,
    )
    return facilities


@router.get("/facilities/search/all/", response_model=List[FacilityRead])
def read_facilities(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    facilities = facility.get_facilities(db, skip=skip, limit=limit)
    return facilities


@router.get("/facilities/search/id/", response_model=FacilityRead)
def read_facility_by_id(
    facility_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_facility = facility.get_facility_by_id(db, id=facility_id)
    if db_facility is None:
        raise HTTPException(status_code=404, detail="Facility not found")
    return db_facility


@router.get("/facilities/search/name/", response_model=FacilityRead)
def read_facility_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_facility = facility.get_facility_by_name(db=db, name=name)
    if db_facility is None:
        raise HTTPException(status_code=404, detail="Facility not found")
    return db_facility


@router.get("/facilities/search/zip_code/", response_model=List[FacilityRead])
def read_facilities_by_zip_code(
    zip_code: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_facilities = facility.get_facilities_by_zip_code(db=db, zip_code=zip_code)
    if db_facilities is None:
        raise HTTPException(status_code=404, detail="Facilities not found")
    return db_facilities


@router.get("/facilities/search/country/", response_model=List[FacilityRead])
def read_facilities_by_country(
    country: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_facilities = facility.get_facilities_by_country(db=db, country=country)
    if db_facilities is None:
        raise HTTPException(status_code=404, detail="Facilities not found")
    return db_facilities


@router.get("/facilities/search/organisation/", response_model=List[FacilityRead])
def read_facility_by_organisation(
    organisation_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_facilities = facility.get_facilities_by_organisation(db=db, id=organisation_id)
    if db_facilities is None:
        raise HTTPException(status_code=404, detail="Facilities not found")
    return db_facilities


@router.put("/facilities/update/id/", response_model=FacilityRead)
def update_facility(
    facility_id: int,
    updated_facility: Facility,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return facility.update_facility(db=db, id=facility_id, facility=updated_facility)


@router.delete("/facilities/delete/id/", response_model=FacilityRead)
def delete_facility(
    facility_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return facility.delete_facility(db=db, id=facility_id)
