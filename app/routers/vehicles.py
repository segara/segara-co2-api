from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import vehicle

from app.schemas.vehicle import Vehicle, VehicleRead

import json


router = APIRouter()


@router.post("/vehicles/create/", response_model=VehicleRead)
def create_vehicle(
    new_vehicle: Vehicle,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle = vehicle.get_vehicle_by_license_plate(
        db, license_plate=new_vehicle.license_plate
    )
    if db_vehicle:
        raise HTTPException(status_code=400, detail="Vehicle already registered")
    return vehicle.create_vehicle(db=db, vehicle=new_vehicle)


@router.post("/vehicles/import/all", response_model=List[VehicleRead])
def import_vehicles(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    vehicles = importer.import_csv(
        file=file,
        schema=Vehicle,
        validate_fn=getattr(vehicle, "get_vehicle_by_license_plate"),
        validate_args="license_plate=new_object.license_plate",
        save_fn=getattr(vehicle, "create_vehicle"),
        save_args="vehicle=record",
        db=db,
    )
    return vehicles


@router.get("/vehicles/search/all/", response_model=List[VehicleRead])
def read_vehicles(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    vehicles = vehicle.get_vehicles(db, skip=skip, limit=limit)
    return vehicles


@router.get("/vehicles/search/id/", response_model=VehicleRead)
def read_vehicle_by_id(
    vehicle_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle = vehicle.get_vehicle_by_id(db, id=vehicle_id)
    if db_vehicle is None:
        raise HTTPException(status_code=404, detail="Vehicle not found")
    return db_vehicle


@router.get("/vehicles/search/license_plate/", response_model=VehicleRead)
def read_vehicle_by_license_plate(
    license_plate: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle = vehicle.get_vehicle_by_license_plate(db, license_plate=license_plate)
    if db_vehicle is None:
        raise HTTPException(status_code=404, detail="Vehicle not found")
    return db_vehicle


@router.get("/vehicles/search/make/", response_model=List[VehicleRead])
def read_vehicles_by_make(
    make: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicles = vehicle.get_vehicles_by_make(db, make=make)
    if db_vehicles is None:
        raise HTTPException(status_code=404, detail="Vehicles not found")
    return db_vehicles


@router.get("/vehicles/search/commercial_name/", response_model=List[VehicleRead])
def read_vehicles_by_commercial_name(
    commercial_name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicles = vehicle.get_vehicles_by_commercial_name(
        db, commercial_name=commercial_name
    )
    if db_vehicles is None:
        raise HTTPException(status_code=404, detail="Vehicles not found")
    return db_vehicles


@router.get("/vehicles/search/year/", response_model=List[VehicleRead])
def read_vehicles_by_year(
    year: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicles = vehicle.get_vehicles_by_car_year(db, car_year=year)
    if db_vehicles is None:
        raise HTTPException(status_code=404, detail="Vehicles not found")
    return db_vehicles


@router.get("/vehicles/search/fleet/", response_model=List[VehicleRead])
def read_vehicles_by_fleet(
    fleet_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicles = vehicle.get_vehicles_by_fleet(db, id=fleet_id)
    if db_vehicles is None:
        raise HTTPException(status_code=404, detail="Vehicles not found")
    return db_vehicles


@router.get("/vehicles/search/emissions_nedc/", response_model=List[VehicleRead])
def read_vehicle_by_emissions_nedc(
    emissions_nedc: str,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_vehicles = vehicle.get_vehicles_by_emissions_nedc(
        db=db, emissions_nedc=emissions_nedc, operator=operator
    )
    if db_vehicles is None:
        raise HTTPException(status_code=404, detail="Vehicles not found")
    return db_vehicles


@router.get("/vehicles/search/emissions_wltp/", response_model=List[VehicleRead])
def read_vehicle_by_emissions_wltp(
    emissions_wltp: str,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_vehicles = vehicle.get_vehicles_by_emissions_wltp(
        db=db, emissions_wltp=emissions_wltp, operator=operator
    )
    if db_vehicles is None:
        raise HTTPException(status_code=404, detail="Vehicles not found")
    return db_vehicles


@router.put("/vehicles/update/id/", response_model=VehicleRead)
def update_vehicle(
    vehicle_id: int,
    updated_vehicle: Vehicle,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return vehicle.update_vehicle(db=db, id=vehicle_id, vehicle=updated_vehicle)


@router.delete("/vehicles/delete/id", response_model=VehicleRead)
def delete_vehicle(
    vehicle_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return vehicle.delete_vehicle(db=db, id=vehicle_id)
