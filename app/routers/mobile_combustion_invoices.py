from datetime import date
from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import mobile_combustion_invoice as invoice
from ..crud import vehicle_type, vehicle

from app.schemas.mobile_combustion_invoice import (
    MobileCombustionInvoice,
    MobileCombustionInvoiceRead,
    MobileCombustionInvoiceCreate,
    MobileCombustionInvoiceDelete,
)

from app.schemas.invoice import Invoice

import json


router = APIRouter()


@router.post(
    "/mobile_combustion_invoices/create/", response_model=MobileCombustionInvoiceRead
)
def create_invoice(
    new_invoice: MobileCombustionInvoiceCreate,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle = vehicle.get_vehicle_by_license_plate(
        db, license_plate=new_invoice.vehicle_license_plate
    )
    db_vehicle_type = vehicle_type.get_vehicle_type_by_name(
        db, name=db_vehicle.vehicle_type_name
    )
    if db_vehicle_type.name == "BEV":
        raise HTTPException(status_code=400, detail="Selected vehicle is electric")
    return invoice.create_invoice(db=db, invoice=new_invoice)


@router.post(
    "/mobile_combustion_invoices/import/all/",
    response_model=List[MobileCombustionInvoiceRead],
)
def import_mobile_combustion_invoices(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    mobile_combustion_invoices = importer.import_csv(
        file=file,
        schema=MobileCombustionInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(invoice, "create_invoice"),
        save_args="invoice=record",
        db=db,
    )
    return mobile_combustion_invoices


@router.get(
    "/mobile_combustion_invoices/search/all/",
    response_model=List[MobileCombustionInvoiceRead],
)
def read_invoices(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    invoices = invoice.get_invoices(db, skip=skip, limit=limit)
    return invoices


@router.get(
    "/mobile_combustion_invoices/search/id/", response_model=MobileCombustionInvoiceRead
)
def read_invoice(
    id: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoice = invoice.get_invoice_by_id(db=db, id=id)
    if db_invoice is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoice


@router.get(
    "/mobile_combustion_invoices/search/number/",
    response_model=MobileCombustionInvoiceRead,
)
def read_invoice_by_invoice_number(
    number: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoice = invoice.get_invoice_by_invoice_number(db=db, invoice_number=number)
    if db_invoice is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoice


@router.get(
    "/mobile_combustion_invoices/search/vehicle/",
    response_model=List[MobileCombustionInvoiceRead],
)
def read_invoice_by_vehicle(
    vehicle_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoices = invoice.get_invoices_by_vehicle_id(db=db, id=vehicle_id)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/mobile_combustion_invoices/search/dates/",
    response_model=List[MobileCombustionInvoiceRead],
)
def read_invoice_between_dates(
    date1: date,
    date2: date,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoices = invoice.get_invoices_between_dates(db=db, date1=date1, date2=date2)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/mobile_combustion_invoices/search/fuel/",
    response_model=List[MobileCombustionInvoiceRead],
)
def read_invoice_by_fuel(
    fuel_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoices = invoice.get_invoices_by_fuel_id(db=db, id=fuel_id)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/mobile_combustion_invoices/search/partial_co2/",
    response_model=List[MobileCombustionInvoiceRead],
)
def read_invoice_by_partial(
    partial: float,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_invoices = invoice.get_invoices_by_partial(
        db=db, partial=partial, operator=operator
    )
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoices not found")
    return db_invoices


@router.put(
    "/mobile_combustion_invoices/edit/id/", response_model=MobileCombustionInvoiceRead
)
def update_invoice(
    invoice_id: int,
    updated_invoice: MobileCombustionInvoice,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return invoice.update_invoice(db=db, id=invoice_id, invoice=updated_invoice)


@router.delete(
    "/mobile_combustion_invoices/delete/id/",
    response_model=MobileCombustionInvoiceDelete,
)
def delete_invoice(
    invoice_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return invoice.delete_invoice(db=db, id=invoice_id)
