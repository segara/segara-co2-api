from datetime import date
from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import fluorocarbon

from app.schemas.fluorocarbon import Fluorocarbon, FluorocarbonRead, FluorocarbonDelete

import json


router = APIRouter()


@router.post("/fluorocarbons/create/", response_model=FluorocarbonRead)
def create_fluorocarbon(
    new_fluorocarbon: Fluorocarbon,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return fluorocarbon.create_fluorocarbon(db=db, fluorocarbon=new_fluorocarbon)


@router.post("/fluorocarbons/import/all/", response_model=List[FluorocarbonRead])
def import_fluorocarbons(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    fluorocarbons = importer.import_csv(
        file=file,
        schema=Fluorocarbon,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(fluorocarbon, "create_fluorocarbon"),
        save_args="fluorocarbon=record",
        db=db,
    )
    return fluorocarbons


@router.get("/fluorocarbons/search/all/", response_model=List[FluorocarbonRead])
def read_fluorocarbons(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    fluorocarbons = fluorocarbon.get_fluorocarbons(db, skip=skip, limit=limit)
    return fluorocarbons


@router.get("/fluorocarbons/search/id/", response_model=FluorocarbonRead)
def read_fluorocarbon(
    id: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fluorocarbon = fluorocarbon.get_fluorocarbon_by_id(db=db, id=id)
    if db_fluorocarbon is None:
        raise HTTPException(status_code=404, detail="Fluorocarbon not found")
    return db_fluorocarbon


@router.get("/fluorocarbons/search/equipment/", response_model=List[FluorocarbonRead])
def read_fluorocarbon_by_equipment(
    equipment_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fluorocarbons = fluorocarbon.get_fluorocarbons_by_equipment_id(
        db=db, id=equipment_id
    )
    if db_fluorocarbons is None:
        raise HTTPException(status_code=404, detail="Fluorocarbon not found")
    return db_fluorocarbons


@router.get("/fluorocarbons/search/year/", response_model=List[FluorocarbonRead])
def read_fluorocarbons_by_year(
    year: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fluorocarbons = fluorocarbon.get_fluorocarbons_by_year(db=db, year=year)
    if db_fluorocarbons is None:
        raise HTTPException(status_code=404, detail="Fluorocarbons not found")
    return db_fluorocarbons


@router.get("/fluorocarbons/search/partial_co2/", response_model=List[FluorocarbonRead])
def read_fluorocarbon_by_partial(
    partial: float,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_fluorocarbons = fluorocarbon.get_fluorocarbons_by_partial(
        db=db, partial=partial, operator=operator
    )
    if db_fluorocarbons is None:
        raise HTTPException(status_code=404, detail="Fluorocarbons not found")
    return db_fluorocarbons


@router.put("/fluorocarbons/edit/id/", response_model=FluorocarbonRead)
def update_fluorocarbon(
    fluorocarbon_id: int,
    updated_fluorocarbon: Fluorocarbon,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return fluorocarbon.update_fluorocarbon(
        db=db, id=fluorocarbon_id, fluorocarbon=updated_fluorocarbon
    )


@router.delete("/fluorocarbons/delete/id/", response_model=FluorocarbonDelete)
def delete_fluorocarbon(
    fluorocarbon_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return fluorocarbon.delete_fluorocarbon(db=db, id=fluorocarbon_id)
