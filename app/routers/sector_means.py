from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import sector_mean

from app.schemas.sector_mean import SectorMean, SectorMeanRead

import json


router = APIRouter()


@router.post("/sector_means/create/", response_model=SectorMeanRead)
def create_sector_mean(
    new_sector_mean: SectorMean,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector_mean = sector_mean.get_sector_mean_by_name_and_year(
        db, sector_name=new_sector_mean.sector_name, year=new_sector_mean.year
    )
    if db_sector_mean:
        raise HTTPException(status_code=400, detail="Sector Mean already registered")
    return sector_mean.create_sector_mean(db=db, sector_mean=new_sector_mean)


@router.post("/sector_means/import/all", response_model=List[SectorMeanRead])
def import_sector_means(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    sector_means = importer.import_csv(
        file=file,
        schema=SectorMean,
        validate_fn=getattr(sector_mean, "get_sector_mean_by_name_and_year"),
        validate_args="sector_name=new_object.sector_name,year=new_object.year",
        save_fn=getattr(sector_mean, "create_sector_mean"),
        save_args="sector_mean=record",
        db=db,
    )
    return sector_means


@router.get("/sector_means/search/all/", response_model=List[SectorMeanRead])
def read_sector_means(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    sector_means = sector_mean.get_sector_means(db, skip=skip, limit=limit)
    return sector_means


@router.get("/sector_means/search/id/", response_model=SectorMeanRead)
def read_sector_mean(
    sector_mean_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector_mean = sector_mean.get_sector_mean_by_id(db, id=sector_mean_id)
    if db_sector_mean is None:
        raise HTTPException(status_code=404, detail="Sector Mean not found")
    return db_sector_mean


@router.get("/sector_means/search/year/", response_model=List[SectorMeanRead])
def read_sector_means_by_year(
    year: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector_means = sector_mean.get_sector_means_by_year(db=db, year=year)
    if db_sector_means is None:
        raise HTTPException(status_code=404, detail="Sector Means not found")
    return db_sector_means


@router.get("/sector_means/search/name/", response_model=List[SectorMeanRead])
def read_sector_means_by_name(
    sector_name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector_means = sector_mean.get_sector_means_by_name(
        db=db, sector_name=sector_name
    )
    if db_sector_means is None:
        raise HTTPException(status_code=404, detail="Sector Means not found")
    return db_sector_means


@router.get("/sector_means/search/name/", response_model=List[SectorMeanRead])
def read_sector_mean_by_name_and_year(
    sector_name: str,
    year: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector_mean = sector_mean.get_sector_mean_by_name_and_year(
        db=db, sector_name=sector_name, year=year
    )
    if db_sector_mean is None:
        raise HTTPException(status_code=404, detail="Sector Mean not found")
    return db_sector_mean


@router.put("/sector_means/update/id/", response_model=SectorMeanRead)
def update_sector_mean(
    sector_mean_id: int,
    updated_sector_mean: SectorMean,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return sector_mean.update_sector_mean(
        db=db, id=sector_mean_id, sector_mean=updated_sector_mean
    )


@router.delete("/sector_means/delete/id/", response_model=SectorMeanRead)
def delete_sector_mean(
    sector_mean_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return sector_mean.delete_sector_mean(db=db, id=sector_mean_id)
