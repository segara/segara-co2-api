from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import vehicle_type

from app.schemas.vehicle_type import VehicleType, VehicleTypeRead

import json


router = APIRouter()


@router.post("/vehicle_types/create/", response_model=VehicleTypeRead)
def create_vehicle_type(
    new_vehicle_type: VehicleType,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle_type = vehicle_type.get_vehicle_type_by_name(
        db, name=new_vehicle_type.name
    )
    if db_vehicle_type:
        raise HTTPException(status_code=400, detail="Vehicle Type already registered")
    return vehicle_type.create_vehicle_type(db=db, vehicle_type=new_vehicle_type)


@router.post("/vehicle_types/import/all", response_model=List[VehicleTypeRead])
def import_vehicle_types(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    vehicle_types = importer.import_csv(
        file=file,
        schema=VehicleType,
        validate_fn=getattr(vehicle_type, "get_vehicle_type_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(vehicle_type, "create_vehicle_type"),
        save_args="vehicle_type=record",
        db=db,
    )
    return vehicle_types


@router.get("/vehicle_types/search/all/", response_model=List[VehicleTypeRead])
def read_vehicle_types(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    vehicle_types = vehicle_type.get_vehicle_types(db, skip=skip, limit=limit)
    return vehicle_types


@router.get("/vehicle_types/search/id/", response_model=VehicleTypeRead)
def read_vehicle_type(
    vehicle_type_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle_type = vehicle_type.get_vehicle_type_by_id(db, id=vehicle_type_id)
    if db_vehicle_type is None:
        raise HTTPException(status_code=404, detail="Vehicle Type not found")
    return db_vehicle_type


@router.get("/vehicle_types/search/name/", response_model=VehicleTypeRead)
def read_vehicle_type_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_vehicle_type = vehicle_type.get_vehicle_type_by_name(db, name=name)
    if db_vehicle_type is None:
        raise HTTPException(status_code=404, detail="VehicleType not found")
    return db_vehicle_type


@router.put("/vehicle_types/update/id/", response_model=VehicleTypeRead)
def update_vehicle_type(
    vehicle_type_id: int,
    updated_vehicle_type: VehicleType,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return vehicle_type.update_vehicle_type(
        db=db, id=vehicle_type_id, vehicle_type=updated_vehicle_type
    )


@router.delete("/vehicle_types/delete/id/", response_model=VehicleTypeRead)
def delete_vehicle_type(
    vehicle_type_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return vehicle_type.delete_vehicle_type(db=db, id=vehicle_type_id)
