from datetime import date
from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import stationary_combustion_invoice as invoice

from app.schemas.stationary_combustion_invoice import (
    StationaryCombustionInvoiceRead,
    StationaryCombustionInvoice,
    StationaryCombustionInvoiceCreate,
    StationaryCombustionInvoiceDelete,
)

import json


router = APIRouter()


@router.post(
    "/stationary_combustion_invoices/create/",
    response_model=StationaryCombustionInvoiceRead,
)
def create_invoice(
    new_invoice: StationaryCombustionInvoiceCreate,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return invoice.create_invoice(db=db, invoice=new_invoice)


@router.post(
    "/stationary_combustion_invoices/import/all",
    response_model=List[StationaryCombustionInvoiceRead],
)
def import_stationary_combustion_invoices(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    stationary_combustion_invoices = importer.import_csv(
        file=file,
        schema=StationaryCombustionInvoice,
        validate_fn=None,
        validate_args=None,
        save_fn=getattr(invoice, "create_invoice"),
        save_args="invoice=record",
        db=db,
    )
    return stationary_combustion_invoices


@router.get(
    "/stationary_combustion_invoices/search/all/",
    response_model=List[StationaryCombustionInvoiceRead],
)
def read_invoices(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    invoices = invoice.get_invoices(db, skip=skip, limit=limit)
    return invoices


@router.get(
    "/stationary_combustion_invoices/search/id/",
    response_model=StationaryCombustionInvoiceRead,
)
def read_invoice(
    id: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoice = invoice.get_invoice_by_id(db=db, id=id)
    if db_invoice is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoice


@router.get(
    "/stationary_combustion_invoices/search/number/",
    response_model=StationaryCombustionInvoiceRead,
)
def read_invoice_by_invoice_number(
    number: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoice = invoice.get_invoice_by_invoice_number(db=db, number=number)
    if db_invoice is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoice


@router.get(
    "/stationary_combustion_invoices/search/facility/",
    response_model=List[StationaryCombustionInvoiceRead],
)
def read_invoice_by_facility(
    facility_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoices = invoice.get_invoices_by_facility_id(db=db, id=facility_id)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/stationary_combustion_invoices/search/dates/",
    response_model=List[StationaryCombustionInvoiceRead],
)
def read_invoice_between_dates(
    date1: date,
    date2: date,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoices = invoice.get_invoices_between_dates(db=db, date1=date1, date2=date2)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/stationary_combustion_invoices/search/fuel/",
    response_model=List[StationaryCombustionInvoiceRead],
)
def read_invoice_by_fuel(
    fuel_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_invoices = invoice.get_invoices_by_fuel_id(db=db, id=fuel_id)
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoice not found")
    return db_invoices


@router.get(
    "/stationary_combustion_invoices/search/partial_co2/",
    response_model=List[StationaryCombustionInvoiceRead],
)
def read_invoice_by_partial(
    partial: float,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_invoices = invoice.get_invoices_by_partial(
        db=db, partial=partial, operator=operator
    )
    if db_invoices is None:
        raise HTTPException(status_code=404, detail="Invoices not found")
    return db_invoices


@router.put(
    "/stationary_combustion_invoices/edit/id/",
    response_model=StationaryCombustionInvoiceRead,
)
def update_invoice(
    invoice_id: int,
    updated_invoice: StationaryCombustionInvoice,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return invoice.update_invoice(db=db, id=invoice_id, invoice=updated_invoice)


@router.delete(
    "/stationary_combustion_invoices/delete/id/",
    response_model=StationaryCombustionInvoiceDelete,
)
def delete_invoice(
    invoice_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return invoice.delete_invoice(db=db, id=invoice_id)
