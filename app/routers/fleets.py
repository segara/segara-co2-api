from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import fleet

from app.schemas.fleet import FleetRead, Fleet

import json


router = APIRouter()


@router.post("/fleets/create/", response_model=FleetRead)
def create_fleet(
    new_fleet: Fleet,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fleet = fleet.get_fleet_by_name(db, name=new_fleet.name)
    if db_fleet:
        raise HTTPException(status_code=400, detail="Fleet already registered")
    return fleet.create_fleet(db=db, fleet=new_fleet)


@router.post("/fleets/import/all", response_model=List[FleetRead])
def import_fleets(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    fleets = importer.import_csv(
        file=file,
        schema=Fleet,
        validate_fn=getattr(fleet, "get_fleet_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(fleet, "create_fleet"),
        save_args="fleet=record",
        db=db,
    )
    return fleets


@router.get("/fleets/search/all/", response_model=List[Fleet])
def read_fleets(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
):
    fleets = fleet.get_fleets(db, skip=skip, limit=limit)
    return fleets


@router.get("/fleets/search/id/", response_model=FleetRead)
def read_fleet(
    fleet_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fleet = fleet.get_fleet_by_id(db, id=fleet_id)
    if db_fleet is None:
        raise HTTPException(status_code=404, detail="Fleet not found")
    return db_fleet


@router.get("/fleets/search/name/", response_model=FleetRead)
def read_fleet_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fleet = fleet.get_fleet_by_name(db=db, name=name)
    if db_fleet is None:
        raise HTTPException(status_code=404, detail="Fleet not found")
    return db_fleet


@router.get("/fleets/search/organisation/", response_model=List[FleetRead])
def read_fleet_by_organisation(
    organisation_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_fleets = fleet.get_fleets_by_organisation(db=db, id=organisation_id)
    if db_fleets is None:
        raise HTTPException(status_code=404, detail="Fleets not found")
    return db_fleets


@router.put("/fleets/update/id/", response_model=FleetRead)
def update_fleet(
    fleet_id: int, updated_fleet: Fleet, db: Session = Depends(dependencies.get_db)
) -> Any:
    return fleet.update_fleet(db=db, id=fleet_id, fleet=updated_fleet)


@router.delete("/fleets/delete/id/", response_model=FleetRead)
def delete_fleet(
    fleet_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return fleet.delete_fleet(db=db, id=fleet_id)
