from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import supplier

from app.schemas.supplier import Supplier, SupplierRead

import json

router = APIRouter()


@router.post("/suppliers/create/", response_model=SupplierRead)
def create_supplier(
    new_supplier: Supplier,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_supplier = supplier.get_supplier_by_name_and_year(
        db, name=new_supplier.name, year=new_supplier.year
    )
    if db_supplier:
        raise HTTPException(status_code=400, detail="Supplier already registered")
    return supplier.create_supplier(db=db, supplier=new_supplier)


@router.post("/suppliers/import/all", response_model=List[SupplierRead])
def import_suppliers(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    suppliers = importer.import_csv(
        file=file,
        schema=Supplier,
        validate_fn=getattr(supplier, "get_supplier_by_name_and_year"),
        validate_args="name=new_object.name,year=new_object.year",
        save_fn=getattr(supplier, "create_supplier"),
        save_args="supplier=record",
        db=db,
    )
    return suppliers


@router.get("/suppliers/search/all/", response_model=List[SupplierRead])
def read_suppliers(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    suppliers = supplier.get_suppliers(db, skip=skip, limit=limit)
    return suppliers


@router.get("/suppliers/search/id/", response_model=SupplierRead)
def read_supplier(
    supplier_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_supplier = supplier.get_supplier_by_id(db, id=supplier_id)
    if db_supplier is None:
        raise HTTPException(status_code=404, detail="Supplier not found")
    return db_supplier


@router.get("/suppliers/search/name/", response_model=List[SupplierRead])
def read_supplier_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_supplier = supplier.get_supplier_by_name(db=db, name=name)
    if db_supplier is None:
        raise HTTPException(status_code=404, detail="Supplier not found")
    return db_supplier


@router.get("/suppliers/search/year/", response_model=List[SupplierRead])
def read_suppliers_by_year(
    year: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_suppliers = supplier.get_suppliers_by_year(db=db, year=year)
    if db_suppliers is None:
        raise HTTPException(status_code=404, detail="Supplier not found")
    return db_suppliers


@router.get("/suppliers/search/goo/", response_model=List[SupplierRead])
def read_suppliers_by_guarantee_of_origin(
    guarantee_of_origin: bool,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_suppliers = supplier.get_suppliers_by_guarantee_of_origin(
        db=db, guarantee_of_origin=guarantee_of_origin
    )
    if db_suppliers is None:
        raise HTTPException(status_code=404, detail="Supplier not found")
    return db_suppliers


@router.get("/suppliers/search/emission_factor/", response_model=List[SupplierRead])
def read_suppliers_by_emission_factor(
    emission_factor: str,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_suppliers = supplier.get_suppliers_by_emission_factor(
        db=db, emission_factor=emission_factor, operator=operator
    )
    if db_suppliers is None:
        raise HTTPException(status_code=404, detail="Supplier not found")
    return db_suppliers


@router.put("/suppliers/update/id/", response_model=SupplierRead)
def update_supplier(
    supplier_id: int,
    updated_supplier: Supplier,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return supplier.update_supplier(db=db, id=supplier_id, supplier=updated_supplier)


@router.delete("/suppliers/delete/id", response_model=SupplierRead)
def delete_supplier(
    supplier_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return supplier.delete_supplier(db=db, id=supplier_id)
