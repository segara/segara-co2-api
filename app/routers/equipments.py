from typing import List, Any

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from ..crud import equipment
from ..core import dependencies, models
from .login import oauth2_scheme

from app.schemas.equipment import Equipment, EquipmentRead

import json


router = APIRouter()


@router.post("/equipments/create/", response_model=EquipmentRead)
def create_equipment(
    new_equipment: Equipment,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipment = equipment.get_equipment_by_nickname(
        db, nickname=new_equipment.nickname
    )
    if db_equipment:
        raise HTTPException(status_code=400, detail="Equipment already registered")
    return equipment.create_equipment(db=db, equipment=new_equipment)


@router.post("/equipments/import/all", response_model=List[EquipmentRead])
def import_equipments(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    equipments = importer.import_csv(
        file=file,
        schema=Equipment,
        validate_fn=getattr(equipment, "get_equipment_by_nickname"),
        validate_args="nickname=new_object.nickname",
        save_fn=getattr(equipment, "create_equipment"),
        save_args="equipment=record",
        db=db,
    )
    return equipments


@router.get("/equipments/search/all/", response_model=List[EquipmentRead])
def read_equipments(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    equipments = equipment.get_equipments(db, skip=skip, limit=limit)
    return equipments


@router.get("/equipments/search/id/", response_model=EquipmentRead)
def read_equipment_by_id(
    equipment_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipment = equipment.get_equipment_by_id(db, id=equipment_id)
    if db_equipment is None:
        raise HTTPException(status_code=404, detail="Equipment not found")
    return db_equipment


@router.get("/equipments/search/nickname/", response_model=EquipmentRead)
def read_equipment_by_nickname(
    nickname: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipment = equipment.get_equipment_by_nickname(db, nickname=nickname)
    if db_equipment is None:
        raise HTTPException(status_code=404, detail="Equipment not found")
    return db_equipment


@router.get("/equipments/search/equipment_type_id/", response_model=List[EquipmentRead])
def read_equipments_by_equipment_type(
    id: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipments = equipment.get_equipments_by_equipment_type_id(db, id=id)
    if db_equipments is None:
        raise HTTPException(status_code=404, detail="Equipments not found")
    return db_equipments


@router.put("/equipments/update/id/", response_model=EquipmentRead)
def update_equipment(
    equipment_id: int,
    updated_equipment: Equipment,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return equipment.update_equipment(
        db=db, id=equipment_id, equipment=updated_equipment
    )


@router.delete("/equipments/delete/id", response_model=EquipmentRead)
def delete_equipment(
    equipment_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return equipment.delete_equipment(db=db, id=equipment_id)
