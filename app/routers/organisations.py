from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import organisation

from app.schemas.organisation import OrganisationRead, Organisation

import json


router = APIRouter()


@router.post("/organisations/create/", response_model=OrganisationRead)
def create_organisation(
    new_organisation: Organisation,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_organisation = organisation.get_organisation_by_name(
        db, organisation_name=new_organisation.name
    )
    if db_organisation:
        raise HTTPException(status_code=400, detail="Organisation already registered")
    return organisation.create_organisation(db=db, organisation=new_organisation)


@router.post("/organisations/import/all", response_model=List[OrganisationRead])
def import_organisations(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    organisations = importer.import_csv(
        file=file,
        schema=Organisation,
        validate_fn=getattr(organisation, "get_organisation_by_name"),
        validate_args="organisation_name=new_object.name",
        save_fn=getattr(organisation, "create_organisation"),
        save_args="organisation=record",
        db=db,
    )
    return organisations


@router.get("/organisations/search/all/", response_model=List[OrganisationRead])
def read_organisations(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    organisations = organisation.get_organisations(db, skip=skip, limit=limit)
    return organisations


@router.get("/organisations/search/id/", response_model=OrganisationRead)
def read_organisation(
    organisation_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_organisation = organisation.get_organisation_by_id(db, id=organisation_id)
    if db_organisation is None:
        raise HTTPException(status_code=404, detail="Organisation not found")
    return db_organisation


@router.get("/organisations/search/name/", response_model=OrganisationRead)
def read_organisation_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_organisation = organisation.get_organisation_by_name(db, organisation_name=name)
    if db_organisation is None:
        raise HTTPException(status_code=404, detail="Organisation not found")
    return db_organisation


@router.put("/organisations/update/id/", response_model=OrganisationRead)
def update_organisation(
    organisation_id: int,
    updated_organisation: Organisation,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return organisation.update_organisation(
        db=db, id=organisation_id, organisation=updated_organisation
    )


@router.delete("/organisations/delete/id/", response_model=OrganisationRead)
def delete_organisation(
    organisation_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return organisation.delete_organisation(db=db, id=organisation_id)
