from typing import List, Any
from jose import JWTError, jwt
from fastapi import Depends, HTTPException, APIRouter, File, UploadFile, status
from sqlalchemy.orm import Session

from ..core import dependencies, models
from ..core.config import settings
from ..utils import importer
from .login import oauth2_scheme
from ..crud import user

from app.schemas.user import UserCreate, UserRead


router = APIRouter()


@router.get("/users/me", response_model=UserRead)
def get_current_user(
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
    email: str = payload.get("sub")
    current_user = user.get_user_by_email(email=email, db=db)
    return current_user


@router.post("/users/create", response_model=UserRead)
def create_user(
    new_user: UserCreate,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_user = user.get_user_by_username(db, username=new_user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="User already registered")
    return user.create_user(db=db, user=new_user)


@router.post("/users/import/all", response_model=List[UserRead])
def import_users(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    users = importer.import_csv(
        file=file,
        schema=user,
        validate_fn=getattr(user, "get_user_by_username"),
        validate_args="username=new_object.username",
        save_fn=getattr(user, "create_user"),
        save_args="user=record",
        db=db,
    )
    return users


@router.get("/users/search/all/", response_model=List[UserRead])
def read_users(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    users = user.get_users(db, skip=skip, limit=limit)
    return users


@router.get("/users/search/id/", response_model=UserRead)
def read_user(
    user_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_user = user.get_user_by_id(db, id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@router.get("/users/search/username/", response_model=UserRead)
def read_user_by_username(
    username: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_user = user.get_user_by_username(db, username=username)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@router.put("/users/update/id/", response_model=UserRead)
def update_user(
    user_id: int, updated_user: UserCreate, db: Session = Depends(dependencies.get_db)
) -> Any:
    return user.update_user(db=db, id=user_id, user=updated_user)


@router.delete("/users/delete/id/", response_model=UserRead)
def delete_user(
    user_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return user.delete_user(db=db, id=user_id)
