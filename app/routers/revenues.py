from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import revenue

from app.schemas.revenue import Revenue, RevenueRead

import json


router = APIRouter()


@router.post("/revenues/create/", response_model=RevenueRead)
def create_revenue(
    new_revenue: Revenue,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_revenue = revenue.get_revenue_by_year(db, year=new_revenue.year)
    if db_revenue:
        raise HTTPException(
            status_code=400, detail=f"Revenue already registered for year {year}"
        )
    return revenue.create_revenue(db=db, revenue=new_revenue)


@router.post("/revenues/import/all", response_model=List[RevenueRead])
def import_revenues(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    revenues = importer.import_csv(
        file=file,
        schema=Revenue,
        validate_fn=getattr(revenue, "get_revenue_by_year"),
        validate_args="year=new_object.year",
        save_fn=getattr(revenue, "create_revenue"),
        save_args="revenue=record",
        db=db,
    )
    return revenues


@router.get("/revenues/search/all/", response_model=List[RevenueRead])
def read_revenues(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    revenues = revenue.get_revenues(db, skip=skip, limit=limit)
    return revenues


@router.get("/revenues/search/id/", response_model=RevenueRead)
def read_revenue(
    revenue_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_revenue = revenue.get_revenue_by_id(db, id=revenue_id)
    if db_revenue is None:
        raise HTTPException(status_code=404, detail="Revenue not found")
    return db_revenue


@router.get("/revenues/search/year/", response_model=RevenueRead)
def read_revenue_by_year(
    year: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_revenue = revenue.get_revenue_by_year(db, year=year)
    if db_revenue is None:
        raise HTTPException(status_code=404, detail="Revenue not found")
    return db_revenue


@router.put("/revenues/update/id/", response_model=RevenueRead)
def update_revenue(
    revenue_id: int,
    updated_revenue: Revenue,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return revenue.update_revenue(db=db, id=revenue_id, revenue=updated_revenue)


@router.delete("/revenues/delete/id/", response_model=RevenueRead)
def delete_revenue(
    revenue_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return revenue.delete_revenue(db=db, id=revenue_id)
