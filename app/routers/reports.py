import os
import pdfkit
from fastapi import APIRouter, Request, Depends
from fastapi.responses import HTMLResponse, FileResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from starlette.background import BackgroundTasks

from .login import oauth2_scheme
from ..core import models
from ..core import dependencies
from ..utils.report import StandardReport
from app.schemas.report import Report


router = APIRouter()

templates = Jinja2Templates(directory="app/templates")
template = templates.get_template("report.jinja2")


def remove_file(path: str) -> None:
    for f in os.listdir(path):
        os.remove(os.path.join(path, f))


@router.post("/report/", response_class=FileResponse)
def create_report(
    background_tasks: BackgroundTasks,
    request: Request,
    new_report: Report,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
):
    report = StandardReport(
        db,
        simile_name=new_report.simile_name,
        year=new_report.year,
        organisation_name=new_report.organisation_name,
    )

    tmp = "app/static/tmp/"

    html = template.render(
        request=request,
        organisation=report.organisation_name,
        comparison=report.comparison(),
        simile=report.simile_name,
        year=report.year,
        total=report.formatted_total,
        results=report.results,
        distribution=report.distribution(),
        # yearly_evolution: report.yearly_evolution(),
        base_year_comparison=report.base_year_comparison(),
        base_year=report.base_year,
        base_year_revenue_comparison=report.base_year_revenue_comparison(),
        sector=report.organisation.sector_name,
        sector_table=report.sector_table(),
        sector_chart=report.sector_chart(),
    )
    file = open(f"{tmp}report.html", "w")
    file.write(html)
    file.close()

    pdfkit.from_file(f"{tmp}report.html", f"{tmp}file.pdf")

    background_tasks.add_task(remove_file, tmp)

    return FileResponse(
        f"{tmp}file.pdf", media_type="application/pdf", filename="Report"
    )
