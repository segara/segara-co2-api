from typing import List, Any

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from ..crud import equipment_type
from .login import oauth2_scheme
from ..core import dependencies, models

from app.schemas.equipment_type import EquipmentTypeRead, EquipmentType

import json


router = APIRouter()


@router.post("/equipment_types/create/", response_model=EquipmentTypeRead)
def create_equipment_type(
    new_equipment_type: EquipmentType,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipment_type = equipment_type.get_equipment_type_by_name(
        db, name=new_equipment_type.name
    )
    if db_equipment_type:
        raise HTTPException(status_code=400, detail="Equipment type already registered")
    return equipment_type.create_equipment_type(
        db=db, equipment_type=new_equipment_type
    )


@router.post("/equipment_types/import/all", response_model=List[EquipmentTypeRead])
def import_equipment_types(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    equipment_types = importer.import_csv(
        file=file,
        schema=EquipmentType,
        validate_fn=getattr(equipment_type, "get_equipment_type_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(equipment_type, "create_equipment_type"),
        save_args="equipment_type=record",
        db=db,
    )
    return equipment_types


@router.get("/equipment_types/search/all/", response_model=List[EquipmentTypeRead])
def read_equipment_types(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    equipment_types = equipment_type.get_equipment_types(db, skip=skip, limit=limit)
    return equipment_types


@router.get("/equipment_types/search/id/", response_model=EquipmentTypeRead)
def read_equipment_type(
    equipment_type_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipment_type = equipment_type.get_equipment_type_by_id(
        db, id=equipment_type_id
    )
    if db_equipment_type is None:
        raise HTTPException(status_code=404, detail="Equipment type not found")
    return db_equipment_type


@router.get("/equipment_types/search/name/", response_model=EquipmentTypeRead)
def read_equipment_type_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_equipment_type = equipment_type.get_equipment_type_by_name(db, name=name)
    if db_equipment_type is None:
        raise HTTPException(status_code=404, detail="Equipment type not found")
    return db_equipment_type


@router.put("/equipment_types/update/id/", response_model=EquipmentTypeRead)
def update_equipment_type(
    equipment_type_id: int,
    updated_equipment_type: EquipmentType,
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return equipment_type.update_equipment_type(
        db=db, id=equipment_type_id, equipment_type=updated_equipment_type
    )


@router.delete("/equipment_types/delete/id/", response_model=EquipmentTypeRead)
def delete_equipment_type(
    equipment_type_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return equipment_type.delete_equipment_type(db=db, id=equipment_type_id)
