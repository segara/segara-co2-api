from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import role

from app.schemas.role import Role


router = APIRouter()
