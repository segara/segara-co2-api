from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import sector

from app.schemas.sector import Sector, SectorRead

import json


router = APIRouter()


@router.post("/sectors/create/", response_model=SectorRead)
def create_sector(
    new_sector: Sector,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector = sector.get_sector_by_name(db, name=new_sector.name)
    if db_sector:
        raise HTTPException(status_code=400, detail="Sector already registered")
    return sector.create_sector(db=db, sector=new_sector)


@router.post("/sectors/import/all", response_model=List[SectorRead])
def import_sectors(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    sectors = importer.import_csv(
        file=file,
        schema=Sector,
        validate_fn=getattr(sector, "get_sector_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(sector, "create_sector"),
        save_args="sector=record",
        db=db,
    )
    return sectors


@router.get("/sectors/search/all/", response_model=List[SectorRead])
def read_sectors(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    sectors = sector.get_sectors(db, skip=skip, limit=limit)
    return sectors


@router.get("/sectors/search/id/", response_model=SectorRead)
def read_sector(
    sector_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector = sector.get_sector_by_id(db, id=sector_id)
    if db_sector is None:
        raise HTTPException(status_code=404, detail="Sector not found")
    return db_sector


@router.get("/sectors/search/name/", response_model=SectorRead)
def read_sector_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_sector = sector.get_sector_by_name(db, name=name)
    if db_sector is None:
        raise HTTPException(status_code=404, detail="Sector not found")
    return db_sector


@router.put("/sectors/update/id/", response_model=SectorRead)
def update_sector(
    sector_id: int, updated_sector: Sector, db: Session = Depends(dependencies.get_db)
) -> Any:
    return sector.update_sector(db=db, id=sector_id, sector=updated_sector)


@router.delete("/sectors/delete/id/", response_model=SectorRead)
def delete_sector(
    sector_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return sector.delete_sector(db=db, id=sector_id)
