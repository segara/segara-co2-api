from typing import List, Any
from ..core import dependencies, models

from fastapi import Depends, HTTPException, APIRouter, File, UploadFile
from sqlalchemy.orm import Session

from ..utils import importer
from .login import oauth2_scheme
from ..crud import gas

from app.schemas.gas import GasRead, Gas

import json


router = APIRouter()


@router.post("/gases/create/", response_model=GasRead)
def create_gas(
    new_gas: Gas,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_gas = gas.get_gas_by_name(db, name=new_gas.name)
    if db_gas:
        raise HTTPException(status_code=400, detail="Gas already registered")
    return gas.create_gas(db=db, gas=new_gas)


@router.post("/gases/import/all", response_model=List[GasRead])
def import_gases(
    file: UploadFile = File(...),
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    gases = importer.import_csv(
        file=file,
        schema=Gas,
        validate_fn=getattr(gas, "get_gas_by_name"),
        validate_args="name=new_object.name",
        save_fn=getattr(gas, "create_gas"),
        save_args="gas=record",
        db=db,
    )
    return gases


@router.get("/gases/search/all/", response_model=List[GasRead])
def read_gases(
    skip: int = 0,
    limit: int = 100,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    gases = gas.get_gases(db, skip=skip, limit=limit)
    return gases


@router.get("/gases/search/id/", response_model=GasRead)
def read_gas(
    gas_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_gas = gas.get_gas_by_id(db, id=gas_id)
    if db_gas is None:
        raise HTTPException(status_code=404, detail="Gas not found")
    return db_gas


@router.get("/gases/search/name/", response_model=GasRead)
def read_gas_by_name(
    name: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_gas = gas.get_gas_by_name(db=db, name=name)
    if db_gas is None:
        raise HTTPException(status_code=404, detail="Gas not found")
    return db_gas


@router.get("/gases/search/formula/", response_model=GasRead)
def read_gas_by_formula(
    formula: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_gas = gas.get_gas_by_formula(db=db, formula=formula)
    if db_gas is None:
        raise HTTPException(status_code=404, detail="Gas not found")
    return db_gas


@router.get("/gases/search/mixture/", response_model=List[GasRead])
def read_gas_by_mixture(
    mixture: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    db_gases = gas.get_gases_by_mixture(db=db, mixture=mixture)
    if db_gases is None:
        raise HTTPException(status_code=404, detail="Gas not found")
    return db_gases


@router.get("/gases/search/gwp/", response_model=List[GasRead])
def read_gas_by_gwp(
    gwp: str,
    operator: str,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    if operator not in ["et", "lt", "gt"]:
        raise HTTPException(
            status_code=400, detail="Accepted operators: 'gt', 'lt' and 'et'"
        )
    db_gases = gas.get_gases_by_gwp(db=db, gwp=gwp, operator=operator)
    if db_gases is None:
        raise HTTPException(status_code=404, detail="Gas not found")
    return db_gases


@router.put("/gases/update/id/", response_model=GasRead)
def update_gas(
    gas_id: int,
    updated_gas: Gas,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return gas.update_gas(db=db, id=gas_id, gas=updated_gas)


@router.delete("/gases/delete/id", response_model=GasRead)
def delete_gas(
    gas_id: int,
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> Any:
    return gas.delete_gas(db=db, id=gas_id)
