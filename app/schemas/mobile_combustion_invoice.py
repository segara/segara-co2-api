from __future__ import annotations

from datetime import date
from typing import Optional
from pydantic import PositiveFloat, validator

from .invoice import Invoice
from .fuel import Fuel


class MobileCombustionInvoice(Invoice):
    fuel_name: Optional[str] = None
    vehicle_license_plate: Optional[str] = None


class MobileCombustionInvoiceCreate(MobileCombustionInvoice):
    invoice_number: str
    invoice_date: date  
    units: PositiveFloat
    fuel_name: str
    vehicle_license_plate: str


class MobileCombustionInvoiceRead(Invoice):
    id: int
    fuel: Fuel
    vehicle_license_plate: str
    partial_co2: float


class MobileCombustionInvoiceDelete(Invoice):
    id: int
    fuel_id: int
    vehicle_license_plate: str
    partial_co2: float


# Bottom import and update_forward_refs() are used
# to get around circular import. See:
# https://stackoverflow.com/questions/63420889/fastapi-pydantic-circular-references-in-separate-files
# from .vehicle import Vehicle
# MobileCombustionInvoiceRead.update_forward_refs()