from datetime import date
from typing import Optional
from pydantic import BaseModel, validator, conint


class Revenue(BaseModel):
    year: Optional[conint(gt=1900, lt=date.today().year+1)] = None
    total: Optional[float] = None

    class Config:
        orm_mode = True
        

class RevenueCreate(Revenue):
    year: conint(gt=1900, lt=date.today().year+1)
    total: float


class RevenueRead(Revenue):
    id: int