from datetime import date
from typing import Optional
from pydantic import BaseModel, PositiveFloat, confloat, validator

from .driving import DrivingRead


class Route(BaseModel):
    distance: Optional[PositiveFloat] = None
    origin_latitude: Optional[confloat(gt=-90, lt=90)] = None
    origin_longitude: Optional[confloat(gt=-180, lt=180)] = None
    destination_latitude: Optional[confloat(gt=-90, lt=90)] = None
    destination_longitude: Optional[confloat(gt=-180, lt=180)] = None
    route_date: Optional[date] = None
    vehicle_license_plate: Optional[str] = None
    driving_name: Optional[str] = None
    
    class Config:
        orm_mode = True


class RouteCreate(Route):
    distance: PositiveFloat
    route_date: date
    vehicle_license_plate: str
    driving_name: str


class RouteRead(BaseModel):
    id: int
    distance: PositiveFloat
    origin_latitude: Optional[confloat(gt=-90, lt=90)]
    origin_longitude: Optional[confloat(gt=-180, lt=180)]
    destination_latitude: Optional[confloat(gt=-90, lt=90)]
    destination_longitude: Optional[confloat(gt=-180, lt=180)]
    route_date: date
    vehicle_license_plate: str
    driving_name: str
    partial_co2: float
    
    class Config:
        orm_mode = True