from typing import List, Optional
from pydantic import BaseModel, constr, EmailStr


class UserCreate(BaseModel):
    email: Optional[EmailStr] = None
    password: Optional[str] = None
    username: Optional[str] = None
    fullname: Optional[str] = None
    is_active: Optional[bool] = True


class UserRead(BaseModel):
    id: int
    username: str
    email: EmailStr
    fullname: str
    is_active: bool

    class Config:
        orm_mode = True
