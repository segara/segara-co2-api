from __future__ import annotations

from datetime import date
from typing import List, Optional
from pydantic import BaseModel, confloat, conint, constr, validator
from .mobile_electricity_invoice import MobileElectricityInvoiceRead
from .mobile_combustion_invoice import MobileCombustionInvoiceRead
from .route import RouteRead


class Vehicle(BaseModel):
    license_plate: Optional[constr(min_length=1, max_length=12)] = None
    make: Optional[constr(min_length=3, max_length=40)] = None
    commercial_name: Optional[constr(min_length=1, max_length=80)] = None
    version: Optional[constr(min_length=1, max_length=40)] = None
    car_year: Optional[conint(gt=1900, lt=date.today().year+1)] = None
    emissions_nedc: Optional[confloat(ge=0)] = None
    emissions_wltp: Optional[confloat(ge=0)] = None
    fleet_name: Optional[str] = None
    vehicle_type_name: Optional[str] = None
    outliers: Optional[List[float]] = []

    class Config:
        orm_mode = True


class VehicleCreate(Vehicle):
    license_plate: constr(min_length=1, max_length=12)
    make: constr(min_length=3, max_length=40)
    commercial_name: constr(min_length=1, max_length=80)
    version: constr(min_length=1, max_length=40)
    car_year: conint(gt=1900, lt=date.today().year+1)
    fleet_name: str
    vehicle_type_name: str


class VehicleRead(Vehicle):
    id: int
    mobile_combustion_invoices: List[MobileCombustionInvoiceRead] = []
    mobile_electricity_invoices: List[MobileElectricityInvoiceRead] = []
    routes: List[RouteRead] = []


# Bottom import and update_forward_refs() are used
# to get around circular import. See:
# https://stackoverflow.com/questions/63420889/fastapi-pydantic-circular-references-in-separate-files
# from .mobile_combustion_invoice import MobileCombustionInvoiceRead
# VehicleRead.update_forward_refs()