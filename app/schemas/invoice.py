from datetime import date
from typing import Optional
from pydantic import BaseModel, PositiveFloat, validator


class Invoice(BaseModel):
    invoice_number: Optional[str] = None
    invoice_date: Optional[date] = None
    units: Optional[PositiveFloat] = None
    total: Optional[PositiveFloat] = None

    class Config:
        orm_mode = True