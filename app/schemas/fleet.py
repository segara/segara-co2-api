from typing import List, Optional
from pydantic import BaseModel, constr, validator
from . import vehicle


class Fleet(BaseModel):
    name: Optional[constr(min_length=1, max_length=120)] = None
    facility_name: Optional[str] = None

    class Config:
        orm_mode = True


class FleetCreate(Fleet):
    name: constr(min_length=1, max_length=120)
    facility_name: int


class FleetRead(Fleet):
    id: int
    vehicles: List[vehicle.Vehicle] = []