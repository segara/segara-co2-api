from typing import List, Optional
from pydantic import BaseModel, constr, validator
from . import fluorocarbon


class Equipment(BaseModel):
    nickname: Optional[constr(min_length=1, max_length=120)] = None
    facility_name: Optional[str] = None
    equipment_type_name: Optional[str] = None
    outliers: Optional[List[float]] = []
    
    class Config:
        orm_mode = True


class EquipmentCreate(Equipment):
    nickname: constr(min_length=1, max_length=120)
    facility_name: str
    equipment_type_name: str


class EquipmentRead(Equipment):
    id: int
    fluorocarbons: List[fluorocarbon.Fluorocarbon] = []