from typing import Optional
from pydantic import BaseModel, validator


class Sector(BaseModel):
    name: Optional[str] = None

    class Config:
        orm_mode = True
        

class SectorCreate(Sector):
    name: str


class SectorRead(Sector):
    id: int