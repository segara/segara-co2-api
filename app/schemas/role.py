from typing import Optional
from pydantic import BaseModel, constr, validator


class Role(BaseModel):
    id: Optional[int] = None
    name: constr(min_length=1, max_length=20)

    class Config:
        orm_mode = True