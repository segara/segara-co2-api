from datetime import date
from typing import Optional
from pydantic import BaseModel, conint, validator

from .gas import Gas


class Fluorocarbon(BaseModel):
    annual_refilling: Optional[float] = None
    year: Optional[conint(gt=1900, lt=date.today().year+1)] = None
    equipment_nickname: Optional[str] = None
    gas_name: Optional[str] = None

    class Config:
        orm_mode = True


class FluorocarbonCreate(Fluorocarbon):
    annual_refilling: float
    year: conint(gt=1900, lt=date.today().year+1)
    equipment_nickname: str
    gas_name: str


class FluorocarbonRead(BaseModel):
    id: int
    annual_refilling: float
    year: conint(gt=1900, lt=date.today().year+1)
    equipment_nickname: str
    gas: Gas
    partial_co2: float

    class Config:
        orm_mode = True


class FluorocarbonDelete(BaseModel):
    id: int
    annual_refilling: float
    year: int
    equipment_nickname: str
    gas_id: int
    partial_co2: float

    class Config:
        orm_mode = True