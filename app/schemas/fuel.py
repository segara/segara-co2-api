from datetime import date
from typing import Optional
from pydantic import BaseModel, constr, conint, validator


class Fuel(BaseModel):
    name: Optional[constr(min_length=1, max_length=40)] = None
    year: Optional[conint(gt=1900, lt=date.today().year+1)] = None
    emission_factor: Optional[float] = None

    class Config:
        orm_mode = True

    
class FuelCreate(Fuel):
    name: constr(min_length=1, max_length=40)
    year: conint(gt=1900, lt=date.today().year+1)
    emission_factor: float


class FuelRead(Fuel):
    id: int