from datetime import date
from typing import Optional
from pydantic import PositiveFloat, validator

from .invoice import Invoice
from .supplier import Supplier


class StationaryElectricityInvoice(Invoice):
    supplier_name: Optional[str] = None
    facility_name: Optional[str] = None


class StationaryElectricityInvoiceCreate(StationaryElectricityInvoice):
    invoice_number: str
    invoice_date: date  
    units: PositiveFloat
    supplier_name: str
    facility_name: str


class StationaryElectricityInvoiceRead(Invoice):
    id: int
    supplier: Supplier
    facility_name: str
    partial_co2: float


class StationaryElectricityInvoiceDelete(Invoice):
    id: int
    supplier_id: int
    facility_name: str
    partial_co2: float