from typing import Optional
from pydantic import BaseModel, PositiveFloat, constr, validator


class Gas(BaseModel):
    name: Optional[constr(min_length=1, max_length=20)] = None
    mixture: Optional[bool] = False
    formula: Optional[constr(min_length=1, max_length=120)] = None
    global_warming_potential: Optional[PositiveFloat] = None

    class Config:
        orm_mode = True
        

class GasCreate(Gas):
    name: constr(min_length=1, max_length=20)
    mixture: bool = False
    formula: constr(min_length=1, max_length=120)
    global_warming_potential: PositiveFloat


class GasRead(Gas):
    id: int