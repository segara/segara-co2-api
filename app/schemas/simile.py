from typing import Optional
from pydantic import BaseModel, validator


class Simile(BaseModel):
    name: Optional[str] = None
    volume: Optional[float] = None

    class Config:
        orm_mode = True
        

class SimileCreate(Simile):
    name: str
    volume: float


class SimileRead(Simile):
    id: int