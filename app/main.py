from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
import os

from .core.config import settings
from .core import models
from .core.database import engine
from .routers import (
    drivings,
    equipments,
    equipment_types,
    facilities,
    fleets,
    fluorocarbons,
    fuels,
    gases,
    mobile_combustion_invoices,
    mobile_electricity_invoices,
    organisations,
    roles,
    routes,
    stationary_combustion_invoices,
    stationary_electricity_invoices,
    suppliers,
    users,
    vehicles,
    vehicle_types,
    reports,
    sector_means,
    sectors,
    similes,
    revenues,
    login,
)


models.Base.metadata.create_all(bind=engine)

app = FastAPI()


app.mount("/static", StaticFiles(directory="app/static"), name="static")
if not os.path.exists("app/static/tmp"):
    os.mkdir("app/static/tmp/")

origins = settings.ORIGINS

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(login.router)
app.include_router(drivings.router)
app.include_router(equipments.router)
app.include_router(equipment_types.router)
app.include_router(facilities.router)
app.include_router(fleets.router)
app.include_router(fluorocarbons.router)
app.include_router(fuels.router)
app.include_router(gases.router)
app.include_router(mobile_combustion_invoices.router)
app.include_router(mobile_electricity_invoices.router)
app.include_router(organisations.router)
app.include_router(reports.router)
app.include_router(revenues.router)
app.include_router(roles.router)
app.include_router(routes.router)
app.include_router(sectors.router)
app.include_router(sector_means.router)
app.include_router(similes.router)
app.include_router(stationary_combustion_invoices.router)
app.include_router(stationary_electricity_invoices.router)
app.include_router(suppliers.router)
app.include_router(users.router)
app.include_router(vehicles.router)
app.include_router(vehicle_types.router)


@app.get("/")
async def main():
    return {"message": "Welcome to SegaraCO2 REST API!"}
