# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Users management.
- Role based access control.

### Changed

- Improvements to error handling.
- Improvements to input data validation.

## [0.2.0] - 2021-09-30

### Added

- Reporting module based on Pandas, Matplotlib, Jinja and Bulma.
- Importer module.
- Sector, SectorMeans, Revenue, Simile and VehicleType SQLA and Pydantic models.
- Script to load default data from CSV files.
- Field to define the range of a normal distribution for an instance of a Vehicle, Facility or Equipment, for better outlier detection.

### Changed

- Adjustments in models, endpoints and CRUD utils to integrate with frontend.

### Fixed

- Typos and imports.

## [0.1.0] - 2021-03-23

### Added

- SQLAlchemy models.
- Pydantic models (schemas).
- REST API endpoints.
- CRUD utils package.
- Calculator module.
- Script to create default and example data into the database.
