# Segara CO2 REST API

REST API of the Segara Emission Control System (SegaraCO2).

## Development

Ensure you have a PostgreSQL server running on the same machine.

You may want to change global variables, such as database connection details or default user credentials, in `/app/core/config.py`. If you intend to deploy in production, we highly recommend to provide this data through [environment variables](https://fastapi.tiangolo.com/advanced/settings/).

This project use Poetry for dependency management. Install it following the official [documentation](https://python-poetry.org/docs/#installation).

Execute this command to install all the development dependencies:

```
poetry install
```

Install [wkhtmltopdf](https://github.com/JazzCore/python-pdfkit/wiki/Installing-wkhtmltopdf).

You may need to install the locale to `es_ES`:

```
sudo apt-get install language-pack-es
```

Then run:

```
poetry run uvicorn app.main:app --reload
```

If it is your first time running the app, create default and example data:

```
poetry run python -m app.core.initial_data
```

## Deployment

This app has only been tested in Ubuntu 22.04.

### Prerequisites

Ensure you have installed poetry, wkhtmltopdf and es_ES locale.

### Download and setup the application

Clone the git repository:

```
git clone https://gitlab.com/segara/segara-co2-api.git
```

Go to the project:

```
cd segara-co2-api/
```

Edit `/app/core/config.py` to suit your needs. Keep in mind that you must include the frontend address in `ORIGINGS`, as stated in the [official FastAPI docs](https://fastapi.tiangolo.com/tutorial/cors/).

### Gunicorn

Install gunicorn:

```
poetry add gunicorn
```

Add the configuration file for Gunicorn:

```
nano gunicorn_conf.py
```

Add this to the new file:

```
from multiprocessing import cpu_count

# Socket Path
bind = '0.0.0.0:8000'

# Worker Options
workers = cpu_count() + 1
worker_class = 'uvicorn.workers.UvicornWorker'

# Logging Options
loglevel = 'debug'
accesslog = '/home/<your-user>/segara-co2-api/access_log'
errorlog =  '/home/<your-user>/segara-co2-api/error_log'
```

This command will tell you the gunicorn path in the new virtual environment:

```
poetry run which gunicorn
```

Create a systemd unit file:

```
sudo nano /etc/systemd/system/segara-co2-api.service
```

Add this to the new file:

```
[Unit]
Description=Gunicorn Daemon for SegaraCO2 FastAPI
After=network.target

[Service]
User=azureuser
Group=azureuser
WorkingDirectory=/home/azureuser/pompadour-backend
ExecStart=<gunicorn-path> -c gunicorn_conf.py app.main:app
[Install]
WantedBy=multi-user.target
```

Start and enable the new service:

```
sudo systemctl start segara-co2-api
sudo systemctl enable segara-co2-api
```

Now you can control your application using the following commands:

```
sudo systemctl start segara-co2-api
sudo systemctl stop segara-co2-api
sudo systemctl restart segara-co2-api
```

### Install and set up Nginx

Install nginx:

```
sudo apt-get update && sudo apt-get install nginx
```

Create a server block for the project:

```
sudo nano /etc/nginx/sites-available/segara-co2-api
```

Add this to the new file:

```
server {
    server_name <your-domain>;
    location / {
        proxy_pass http://0.0.0.0:8000;
    }
}
```

Then link the new server block to sites-enabled:

```
sudo ln -s /etc/nginx/sites-available/segara-co2-api /etc/nginx/sites-enabled/segara-co2-api
```

Restart Nginx:

```
sudo systemctl restart nginx
```

### SSL

Install Certbot:

```
sudo snap install --classic certbot
```

Install a certificate for the app:

```
sudo certbot --nginx
```

### Initial data

If it is your first time running the app, don't forget to create default and example data. Initial data files are located in `/app/static/initial_data`, in case you need to update them before.

Once you're ready, run:

```
poetry run python -m app.core.initial_data
```

We're all set!
